package com.example.duobot.inlab.model;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ENTERPRISE_LAYOUT")
public class EnterpriseLayout {

	@Id
	private UUID layoutId;
	
	@Column(columnDefinition="BINARY(3)")
	private byte[] backgroundColor;
	
	@Column(columnDefinition="BINARY(3)")
	private byte[] titleColor;
	
	@Column(length = 50)
	private String fileNameLogo;
	
	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "ENTERPRISE_LAYOUT_ENTERPRISE_FK"), 
		name = "enterpriseId")
	private Enterprises enterprise;
	
	private LocalDateTime creationDate;
	
	private LocalDateTime modificationDate;
}
