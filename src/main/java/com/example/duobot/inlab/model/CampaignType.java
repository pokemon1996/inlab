package com.example.duobot.inlab.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "CAMPAIGN_TYPE")
public class CampaignType implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 806607951177237936L;

	@Id
	@GeneratedValue
	@Type(type = "org.hibernate.type.UUIDCharType")
	private UUID campaingIdType;

	private String name;
	
	private String description;
	
	private String state;
	
	private String fileNameLogo;
	
	private LocalDateTime creationDate;
	
	private LocalDateTime modificationDate;
	
	@OneToMany(mappedBy = "campaignType")
	private List<Campaign> campaigns;
	
	private Integer clientOrder;
	
}
