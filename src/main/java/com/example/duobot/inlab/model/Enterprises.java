package com.example.duobot.inlab.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ENTERPRISES")
public class Enterprises implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -7410370802887888762L;

	@Id
	@Type(type = "org.hibernate.type.UUIDCharType")
	private UUID enterpriseId;
	
	@Column(length = 20)
	private String identification;
	
	private Integer dv;
	
	@Column(length = 200)
	private String businessName;
	
	@OneToOne(mappedBy = "enterprise")
	private EnterpriseLayout enterpriseLayout;
		
	private LocalDateTime creationDate;
	
	private LocalDateTime modificationDate;
	
}
