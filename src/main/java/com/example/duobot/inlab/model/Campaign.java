package com.example.duobot.inlab.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "CAMPAIGNS")
public class Campaign implements Serializable {

	private static final long serialVersionUID = -4473873499441843415L;

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
	private Integer campaignId;

	@NotNull
	private String campaignName;
		
	private String campaignCustomer;
	
	private String campaignProject;

	private Integer startDate;

	private Integer endDate;

	private LocalDate startCampaignDate;
	
	private LocalDate endCampaignDate;
	
	private LocalDateTime creationDate;
	
	private LocalDateTime modificationDate;
	
	private String videoUrl;

	private String chatUrl;

	private String pollUrl;

	private Integer insightId;

	private String powerBIUrl;

	private String timelineUrl;

	private String tweetsUrl;

	private String topicsUrl;

	private String influenceUrl;

	private String topDomainsUrl;

	private String countries;

	private String demographics;

	private String postType;
	
	private String sources;

	private String assignedUser;
	
	private String pollImageUrl;
	
	private String emotionsUrl;

	private String emotionsIA;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "campaign", cascade = CascadeType.REMOVE)
	@JsonManagedReference
	private List<Gallery> galleries = new ArrayList<Gallery>();

	@OneToMany(mappedBy = "campaign", cascade = CascadeType.REMOVE)
	@JsonManagedReference
	private List<Conference> conferences = new ArrayList<Conference>();

	@ManyToOne
	@JsonIgnore
	@JoinColumn(foreignKey = @ForeignKey(name = "CAMPAIGN_CAMPAIGN_TYPE_FK"),
		name = "campaingIdType")
	private CampaignType campaignType;

	@JsonIgnore
	@OneToMany(mappedBy = "campaign")
	private List<ResourceCampaign> resourceCampaigns;
}
