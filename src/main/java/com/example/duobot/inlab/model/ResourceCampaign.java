package com.example.duobot.inlab.model;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "RESOURCE_CAMPAIGN")
public class ResourceCampaign {

	@Id
	private UUID resourceCampaignId;
	
	@Column(length = 250)
	private String resourceUrl;
	
	private String state;
	
	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "RESOURCE_CAMPAIGN_TYPE_FK"),
		name = "resourceTypeId")
	private ResourceType resourceType;
	
	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "RESOURCE_CAMPAIGN_ID_FK"),
		name = "campaignId")
	private Campaign campaign;
	
	private LocalDateTime creationDate;
	
	private LocalDateTime modificationDate;
}
