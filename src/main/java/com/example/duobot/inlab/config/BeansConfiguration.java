package com.example.duobot.inlab.config;

import org.apache.commons.text.matcher.StringMatcher;
import org.apache.commons.text.matcher.StringMatcherFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeansConfiguration {

	@Bean
	public StringMatcher matcherAnswers() {
		final StringMatcherFactory instanceMatcher = StringMatcherFactory.INSTANCE;
		
		final StringMatcher matcher = instanceMatcher.charSetMatcher(new char[] {' ', ',', '-', ';', '.', ':'});
		
		return matcher;
	}
}
