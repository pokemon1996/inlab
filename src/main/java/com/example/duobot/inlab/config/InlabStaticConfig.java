package com.example.duobot.inlab.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InlabStaticConfig implements WebMvcConfigurer  {
	
	@Value("${services.campaign-type.resource-handler}")
	public String handlerImages;

	@Value("${services.campaign-type.resource-location}")
	public String resourceLocationCampaignType;
	
	@Value("${services.enterprises.resources-location}")
	public String enterprisesResourcesLogo;
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(this.handlerImages)
        	.addResourceLocations("file:///C:/inlab/campaign/")
        	.addResourceLocations(this.resourceLocationCampaignType)
        	.addResourceLocations(this.enterprisesResourcesLogo);
    }
}