package com.example.duobot.inlab.dao;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.duobot.inlab.model.ProhibitedWord;

public interface ProhibitedWordRepository extends JpaRepository<ProhibitedWord, String> {

	@Query(value = "SELECT PW.word FROM ProhibitedWord PW")
	Set<String> findAllSet();

}
