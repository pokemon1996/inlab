package com.example.duobot.inlab.dao;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.duobot.inlab.model.Campaign;
import com.example.duobot.inlab.model.ResourceCampaign;

public interface ResourceCampaignRepository extends JpaRepository<ResourceCampaign, UUID>{

	@Modifying
	@Query("DELETE FROM ResourceCampaign rc WHERE rc.campaign.campaignId = :campaignId")
	void deleteAllByCampaign(@Param("campaignId") Integer campaignId);
	
	List<ResourceCampaign> findByCampaign(Campaign campaign);
	
	@Query(
		"SELECT RC FROM ResourceCampaign RC " + 
		"JOIN RC.campaign C " +
		"JOIN C.campaignType CT " +
		"WHERE CT.campaingIdType = :campaingIdType " + 
		"AND C.assignedUser = :username ")
	List<ResourceCampaign> findByCampaignTypeAndUserName(UUID campaingIdType, String username);
}
