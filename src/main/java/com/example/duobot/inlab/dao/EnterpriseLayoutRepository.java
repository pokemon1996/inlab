package com.example.duobot.inlab.dao;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.duobot.inlab.model.EnterpriseLayout;

public interface EnterpriseLayoutRepository extends JpaRepository<EnterpriseLayout, UUID>{

}
