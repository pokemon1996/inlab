package com.example.duobot.inlab.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.duobot.inlab.model.Enterprises;
import com.example.duobot.inlab.model.User;

public interface InlabUserRepository extends CrudRepository<User, Integer> {
	
	User findByUsername(String username);

	List<User> findByEnterprise(Enterprises enterprise);
}
