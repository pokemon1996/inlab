package com.example.duobot.inlab.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.duobot.inlab.model.Campaign;
import com.example.duobot.inlab.model.CampaignType;

public interface CampaignRepository extends JpaRepository<Campaign, Integer> {
	
	List<Campaign> findByAssignedUserAndEndDateGreaterThanOrderByStartDateDesc(String username, Integer endDate);

	List<Campaign> findByCampaignTypeAndAssignedUserOrderByStartCampaignDateDesc(CampaignType campaignType, String assignedUser);
}
