package com.example.duobot.inlab.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.duobot.inlab.model.Question;

public interface QuestionRepository extends CrudRepository<Question, Integer> {
	
	List<Question> findByPollPollId(Integer pollId);

}
