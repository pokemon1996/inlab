package com.example.duobot.inlab.dao;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.duobot.inlab.model.Enterprises;

public interface EnterprisesRepository extends JpaRepository<Enterprises, UUID>{

	Optional<Enterprises> findByIdentification(String identification);
	
}
