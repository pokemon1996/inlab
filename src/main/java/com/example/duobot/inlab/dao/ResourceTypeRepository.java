package com.example.duobot.inlab.dao;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.duobot.inlab.model.ResourceType;

public interface ResourceTypeRepository extends JpaRepository<ResourceType, UUID>{

}
