package com.example.duobot.inlab.dao;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.duobot.inlab.model.CampaignType;

public interface CampaignTypeRepository extends JpaRepository<CampaignType, UUID>{

	@Query("SELECT CT FROM CampaignType CT ORDER BY CT.clientOrder ASC")
	List<CampaignType> findAllOrderByClientOrderAsc();
	
	@Query(value = "SELECT ISNULL("
			+ "(SELECT TOP 1 1 "
			+ "FROM CR_inlab_usr.CAMPAIGNS CA "
			+ "WHERE CA.assignedUser = :userName "
			+ "AND CA.campaingIdType = :campaingIdType), 0)",
			nativeQuery = true)
	Integer checkCampaignTypeByUser(String userName, String campaingIdType);
}
