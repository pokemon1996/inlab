package com.example.duobot.inlab.dao;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.duobot.inlab.model.Gallery;

public interface GalleryRepository extends CrudRepository<Gallery, Integer> {
	
	List<Gallery> findByCampaignCampaignId(Integer campaignId);

	@Query(
		"SELECT G FROM Gallery G " + 
		"JOIN G.campaign C " +
		"JOIN C.campaignType CT " +
		"WHERE CT.campaingIdType = :campaingIdType " + 
		"AND C.assignedUser = :username ")
	List<Gallery> findByCampaignTypeAndUserName(UUID campaingIdType, String username);
}
 