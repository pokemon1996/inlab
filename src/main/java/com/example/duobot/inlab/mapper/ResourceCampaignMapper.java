package com.example.duobot.inlab.mapper;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.mapstruct.Mapper;

import com.example.duobot.inlab.dto.client.dashboard.CampaignResourceDTORs;
import com.example.duobot.inlab.model.ResourceCampaign;

@Mapper
public abstract class ResourceCampaignMapper {

	public Map<String,List<CampaignResourceDTORs>> mapRersoucesCampaignAsMapCollect(List<ResourceCampaign> resourceCampaigns){
		return Optional.ofNullable(resourceCampaigns)
			.map(resources -> resources.stream()
				.collect(Collectors.groupingBy(resource -> resource.getResourceType().getName(),
					Collectors.mapping(resource -> CampaignResourceDTORs
						.builder()
						.resourceUrl(resource.getResourceUrl())
						.build(), 
						Collectors.toList()))))
			.orElseGet(Collections::emptyMap);
	}
}
