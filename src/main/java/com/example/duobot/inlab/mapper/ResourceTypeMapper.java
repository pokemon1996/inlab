package com.example.duobot.inlab.mapper;

import org.mapstruct.Mapper;

import com.example.duobot.inlab.dto.ResourceTypeDTORs;
import com.example.duobot.inlab.model.ResourceType;

@Mapper
public interface ResourceTypeMapper {

	ResourceTypeDTORs mapFindAllResourceType(ResourceType resourceType);
}
