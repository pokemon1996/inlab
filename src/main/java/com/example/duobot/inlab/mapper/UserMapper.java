package com.example.duobot.inlab.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import com.example.duobot.inlab.dto.UserCreationDTORq;
import com.example.duobot.inlab.dto.UserListDTORs;
import com.example.duobot.inlab.dto.session.UserDTOSessionRs;
import com.example.duobot.inlab.model.User;

@Mapper
public interface UserMapper {

	UserDTOSessionRs mapEntityToDTO(User user);

	User mapCreationUserTOEntity(UserCreationDTORq creationDTORq);
	
	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	User mapModificationUserTOEntity(UserCreationDTORq creationDTORq, @MappingTarget User entity);
	
	UserListDTORs mapUserTOUserDTOList(User user);
	
	@Mapping(target = "roleName", ignore = true)
	@Mapping(target = "enabled", ignore = true)
	@Mapping(target = "enterprise", ignore = true)
	UserListDTORs mapUserTOUserDTOKeyValue(User user);
}
