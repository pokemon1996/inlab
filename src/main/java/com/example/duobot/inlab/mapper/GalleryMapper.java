package com.example.duobot.inlab.mapper;

import org.mapstruct.Mapper;

import com.example.duobot.inlab.dto.GalleryDTORs;
import com.example.duobot.inlab.model.Gallery;

@Mapper
public interface GalleryMapper {

	GalleryDTORs mapEntityTODTO(Gallery gallery);
}
