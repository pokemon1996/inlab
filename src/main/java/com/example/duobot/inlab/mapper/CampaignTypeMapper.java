package com.example.duobot.inlab.mapper;

import org.mapstruct.Mapper;

import com.example.duobot.inlab.dto.CampaignTypeDTORs;
import com.example.duobot.inlab.dto.client.dashboard.CampaignTypeUserDTORs;
import com.example.duobot.inlab.dto.client.dashboard.CampaignUserDTORs;
import com.example.duobot.inlab.model.Campaign;
import com.example.duobot.inlab.model.CampaignType;

@Mapper
public interface CampaignTypeMapper {
	
	CampaignTypeDTORs mapFindAllCampaign(CampaignType campaignType);
	
	CampaignUserDTORs mapCampaignFromType(Campaign campaign);
	
	CampaignTypeUserDTORs mapCampaignTypeByUser(CampaignType campaignType);
	
}
