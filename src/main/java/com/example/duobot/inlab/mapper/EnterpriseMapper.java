package com.example.duobot.inlab.mapper;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.duobot.inlab.constants.CommonConstants;
import com.example.duobot.inlab.dto.EnterprisesDTORq;
import com.example.duobot.inlab.dto.EnterprisesDTORs;
import com.example.duobot.inlab.dto.EnterprisesDetailDTORs;
import com.example.duobot.inlab.model.EnterpriseLayout;
import com.example.duobot.inlab.model.EnterpriseLayout.EnterpriseLayoutBuilder;
import com.example.duobot.inlab.model.Enterprises;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Mapper
public abstract class EnterpriseMapper {

	@Value("${services.campaign-type.resource-handler.folder-name}")
	public String handlerImages;
	
	public abstract Enterprises mapDTOToEntity(EnterprisesDTORq enterprisesDTOIn);
	
	public EnterprisesDTORs mapEntityToDTO(Enterprises enterprises) {
		final EnterprisesDTORs enterprisesDTORs = EnterprisesDTORs.builder()
			.businessName(enterprises.getBusinessName())
			.identification(enterprises.getIdentification())
			.enterpriseId(enterprises.getEnterpriseId())
			.build();
			
			Optional.ofNullable(enterprises.getEnterpriseLayout())
				.ifPresent(layout -> {
					enterprisesDTORs.setBackgroundColorHex(
						CommonConstants.SHARP_CHARACTER + Hex.encodeHexString(layout.getBackgroundColor(), false));
					enterprisesDTORs.setTitleColorHex(
						CommonConstants.SHARP_CHARACTER + Hex.encodeHexString(layout.getTitleColor(), false));
					enterprisesDTORs.setPathLogo(
						ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString() + 
							handlerImages + 
							enterprises.getEnterpriseId() +
							CommonConstants.SLASH_CHARACTER +
							layout.getFileNameLogo());
				});

		return enterprisesDTORs;
	}
	 
	public Enterprises mapDTOToEntityCreation(EnterprisesDTORq enterprisesDTOIn) {
		
		final Enterprises enterprises = this.mapDTOToEntity(enterprisesDTOIn);
				
		enterprises.setEnterpriseId(UUID.randomUUID());
		enterprises.setCreationDate(LocalDateTime.now(ZoneOffset.UTC));
		enterprises.setModificationDate(LocalDateTime.now(ZoneOffset.UTC));
			
		return enterprises;
	}
	
	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	public abstract Enterprises mapModificationEnterpriseTOEntity(EnterprisesDTORq enterprisesDTORq, @MappingTarget Enterprises entity);
	
	public EnterpriseLayout mapEnterpriseLayoutTOEntityCreation(EnterprisesDTORq enterprisesDTOIn) {
		final EnterpriseLayoutBuilder enterpriseLayout = EnterpriseLayout.builder();
		
		try {
			enterpriseLayout
				.backgroundColor(
					Hex.decodeHex(StringUtils.removeStart(enterprisesDTOIn.getBackgroundColorHex(), CommonConstants.SHARP_CHARACTER)))
				.titleColor(
					Hex.decodeHex(StringUtils.removeStart(enterprisesDTOIn.getTitleColorHex(), CommonConstants.SHARP_CHARACTER)));
				
		} catch (DecoderException e) {
			log.error("Setting the default colors...");
			
			try {
				enterpriseLayout
					.backgroundColor(Hex.decodeHex(CommonConstants.DEFAULT_BACKGROUND_COLOR_INLAB))
					.titleColor(Hex.decodeHex(CommonConstants.DEFAULT_TITLE_COLOR_INLAB));
			} catch (DecoderException e1) {
				log.error("Unable to get colors");
			}
		}
		
		return enterpriseLayout
			.layoutId(UUID.randomUUID())
			.creationDate(LocalDateTime.now(ZoneOffset.UTC))
			.modificationDate(LocalDateTime.now(ZoneOffset.UTC))
			.build();
	}
	
	public EnterprisesDetailDTORs mapEnterpriseToEnterpriseDetailDTO(Enterprises enterprises) {
		
		final EnterprisesDetailDTORs detailDTORs = EnterprisesDetailDTORs.builder()
			.businessName(enterprises.getBusinessName())
			.identification(enterprises.getIdentification())
			.build();
			
			Optional.ofNullable(enterprises.getEnterpriseLayout())
				.ifPresent(layout -> {
					detailDTORs.setBackgroundColorHex(
						CommonConstants.SHARP_CHARACTER + Hex.encodeHexString(layout.getBackgroundColor(), false));
					detailDTORs.setTitleColorHex(
						CommonConstants.SHARP_CHARACTER + Hex.encodeHexString(layout.getTitleColor(), false));
					detailDTORs.setPathLogo(
						ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString() + 
							handlerImages + 
							enterprises.getEnterpriseId() +
							CommonConstants.SLASH_CHARACTER +
							layout.getFileNameLogo());
				});

		return detailDTORs;
	}
}
