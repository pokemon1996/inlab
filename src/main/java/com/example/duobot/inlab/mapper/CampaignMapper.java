package com.example.duobot.inlab.mapper;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.beans.factory.annotation.Value;

import com.example.duobot.inlab.dto.CampaignDTORq;
import com.example.duobot.inlab.dto.CampaignListDTORs;
import com.example.duobot.inlab.dto.ResourceCampaignDTORs;
import com.example.duobot.inlab.model.Campaign;

@Mapper
public abstract class CampaignMapper {

	@Value("${services.campaign-type.resource-handler.folder-name}")
	public String handlerImages;
	
	public abstract Campaign mapDtoToEntityCreation(CampaignDTORq campaignDTORq);
	
	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	public abstract Campaign mapDtoToEntityUpdate(CampaignDTORq campaignDTORq, @MappingTarget Campaign entity);
	
	@Mapping(target = "resourceCampaigns", ignore = true)
	public abstract CampaignListDTORs mapListBaseCampaignsTODTO(Campaign campaign);
	
	@Mapping(target = "resourceCampaigns", expression = "java(this.mapListResourcesCampaign(campaign))")
	public abstract CampaignListDTORs mapListCampaignsTODTO(Campaign campaign);

	@Mapping(target = "galleries", ignore = true)
	@Mapping(target = "conferences", ignore = true)
	@Mapping(target = "resourceCampaigns", expression = "java(this.mapListResourcesCampaign(campaign))")
	public abstract CampaignListDTORs mapCampaignDetailEditToDTO(Campaign campaign);
	
	@Mapping(target = "galleries", ignore = true)
	@Mapping(target = "conferences", ignore = true)
	@Mapping(target = "resourceCampaigns", ignore = true)
	public abstract CampaignListDTORs mapCampaignDetailClientViewToDTO(Campaign campaign);
	
	public List<ResourceCampaignDTORs> mapListResourcesCampaign(Campaign campaign){
		return Optional.ofNullable(campaign.getResourceCampaigns())
			.map(resources -> resources.stream()
				.map(resource -> {
					final ResourceCampaignDTORs resourceCampaignDTORs = ResourceCampaignDTORs.builder()
						.resourceUrl(resource.getResourceUrl())
						.resourceTypeId(resource.getResourceType().getResourceTypeId())
						.build();
					
						return resourceCampaignDTORs;
				})
				.collect(Collectors.toList()))
			.orElseGet(Collections::emptyList);
	}
}
