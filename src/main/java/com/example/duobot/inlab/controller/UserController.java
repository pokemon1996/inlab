package com.example.duobot.inlab.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.duobot.inlab.constants.ResourceEndpoints;
import com.example.duobot.inlab.dao.InlabUserRepository;
import com.example.duobot.inlab.dto.UserCreationDTORq;
import com.example.duobot.inlab.model.User;
import com.example.duobot.inlab.service.UserService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class UserController {

	private final InlabUserRepository userRepository;

	private final UserService userService;
	
	@GetMapping(value = "/api/user")
	public ResponseEntity<Object> getUsers() {
		try {
			return ResponseEntity.ok(userService.getListUsers());
		} catch (Exception ex) {
			System.out.println(ex);
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}

	@PostMapping(value = "/api/user")
	public ResponseEntity<Object> addUser(@RequestBody @Valid UserCreationDTORq user) {
		try {
			this.userService.createUser(user);
			
			return ResponseEntity.noContent().build();
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}

	@PutMapping(value = "/api/user/{userId}")
	public ResponseEntity<Object> editUser(
		@RequestBody @Valid UserCreationDTORq user,
		@PathVariable("userId") Integer userId) {
	
		try {
			user.setUserId(userId);
			
			this.userService.updateUser(user);
			
			return ResponseEntity.noContent().build();
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}
	
	@DeleteMapping(value = "/api/user")
	public ResponseEntity<?> deleteUser(@RequestParam Integer userId) {


		try {
			User dbUser = userRepository.findById(userId).get();
			if (dbUser == null) {

				return ResponseEntity.badRequest().body("No encontramos este usuario en el base de datos");
			}
			userRepository.delete(dbUser);
			return ResponseEntity.noContent().build();
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}

	@GetMapping(value = "/api/user/enterprise/{enterprise_id}")
	public ResponseEntity<Object> getUsersByEnterprise(
		@PathVariable(name = ResourceEndpoints.ENTERPRISE_ID) UUID enterpriseId) {
		
		try {
			return ResponseEntity.ok(this.userService.findByEnterprise(enterpriseId));
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}
}
