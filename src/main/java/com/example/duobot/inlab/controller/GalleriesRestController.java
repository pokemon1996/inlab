package com.example.duobot.inlab.controller;

import java.util.UUID;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.duobot.inlab.constants.ResourceEndpoints;
import com.example.duobot.inlab.service.GalleriesService;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping(ResourceEndpoints.GALLERIES_API)
@RequiredArgsConstructor
public class GalleriesRestController {

	private final GalleriesService galleriesService;
	
	@GetMapping(value = ResourceEndpoints.GALLERIES_BY_CAMPAIGN, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getGalleriesByCampaign(
		@PathVariable(name = ResourceEndpoints.CAMPAIGN_ID) Integer campaignId) {
		try {
			return ResponseEntity.ok(this.galleriesService.getGalleriesByCampaign(campaignId));
		} catch (Exception ex) {
			log.error("Error was generated getting the galleries campaign {}", ex.getMessage());
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}
	
	@GetMapping(value = ResourceEndpoints.GALLERIES_BY_CAMPAIGN_TYPE_AND_USER, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getGalleriesByCampaignTypeAndUserName(
		@PathVariable(name = ResourceEndpoints.CAMPAIGN_ID_TYPE) UUID campaignTypeId,
		@PathVariable(name = ResourceEndpoints.USERNAME) String username) {
	
		try {
			return ResponseEntity.ok(this.galleriesService.getGalleriesByCampaignTypeAndUserName(campaignTypeId, username));
		} catch (Exception ex) {
			log.error("Error was generated getting the galleries by campaign type and user {}", ex.getMessage());
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}
}
