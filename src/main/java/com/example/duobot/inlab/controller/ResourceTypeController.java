package com.example.duobot.inlab.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.duobot.inlab.constants.ResourceEndpoints;
import com.example.duobot.inlab.dto.ResourceTypeDTORs;
import com.example.duobot.inlab.service.ResourceTypeService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(ResourceEndpoints.RESOURCE_TYPE_API)
@RequiredArgsConstructor
public class ResourceTypeController {

	private final ResourceTypeService resourceTypeService;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ResourceTypeDTORs>> findAllResourceTypes(){
		return ResponseEntity.ok(this.resourceTypeService.findAllResourceTypes());
	}
}
