package com.example.duobot.inlab.controller;

import java.util.UUID;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.duobot.inlab.constants.ResourceEndpoints;
import com.example.duobot.inlab.service.QuestionService;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequiredArgsConstructor
public class AnswerController {

	private final QuestionService questionService;
	
	@GetMapping(value = ResourceEndpoints.ANSWER_POLL_BY_CAMPAIGN, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAnswersPollByCampaign(Authentication user, 
		@PathVariable(value = ResourceEndpoints.CAMPAIGN_ID) Integer campaignId) {
		try {
			return ResponseEntity.ok(this.questionService.getAnswersPollByCampaign(campaignId));
		} catch (Exception ex) {
			log.error(ex);
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}
	
	@GetMapping(value = ResourceEndpoints.ANSWER_POLL_BY_CAMPAIGN_TYPE_AND_USER, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAnswersPollByCampaignTypeAndUser(Authentication user, 
		@PathVariable(name = ResourceEndpoints.CAMPAIGN_ID_TYPE) UUID campaignTypeId,
		@PathVariable(name = ResourceEndpoints.USERNAME) String username) {
	
		try {
			return ResponseEntity.ok(this.questionService.getAnswersPollByCampaignTypeAndUser(campaignTypeId, username));
		} catch (Exception ex) {
			log.error(ex);
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}
}