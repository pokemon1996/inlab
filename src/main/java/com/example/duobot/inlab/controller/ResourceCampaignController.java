package com.example.duobot.inlab.controller;

import java.util.UUID;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.duobot.inlab.constants.ResourceEndpoints;
import com.example.duobot.inlab.service.ResourceCampaignService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(ResourceEndpoints.RESOURCE_CAMPAIGN_API)
@RequiredArgsConstructor
public class ResourceCampaignController {

	private final ResourceCampaignService resourceCampaignService;
	
	@GetMapping(value = ResourceEndpoints.RESOURCES_BY_CAMPAIGN, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getResourcesCampaignByCampaign(
		@PathVariable(name = ResourceEndpoints.CAMPAIGN_ID) Integer campaignId){
		
		return ResponseEntity.ok(this.resourceCampaignService.getResourcesCampaignByCampaign(campaignId));
	}
	
	@GetMapping(value = ResourceEndpoints.RESOURCES_BY_CAMPAIGN_TYPE_AND_USER, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getResourcesCampaignByCampaignTypeUserAssigned(
		@PathVariable(name = ResourceEndpoints.CAMPAIGN_ID_TYPE) UUID campaignTypeId,
		@PathVariable(name = ResourceEndpoints.USERNAME) String username) {
		
		return ResponseEntity.ok(this.resourceCampaignService.getResourcesCampaignByCampaignTypeUserAssigned(campaignTypeId, username));
	}
}
