package com.example.duobot.inlab.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.duobot.inlab.constants.ResourceEndpoints;
import com.example.duobot.inlab.dto.CampaignTypeDTORs;
import com.example.duobot.inlab.service.CampaignTypeService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(ResourceEndpoints.CAMPAIGN_TYPE_API)
@RequiredArgsConstructor
public class CampaignTypeController {

	private final CampaignTypeService campaignTypeService;
		
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CampaignTypeDTORs>> findAllCampaigns(){
		return ResponseEntity.ok(this.campaignTypeService.findAllCampaigns());
	}
	
	@GetMapping(value = "/user/{userAssigned}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> findCampaignsTypeByUser(
		@PathVariable("userAssigned") String userAssigned){
		
		return ResponseEntity.ok(this.campaignTypeService.findCampaignsTypeByUser(userAssigned));
	}
}
