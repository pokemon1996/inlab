package com.example.duobot.inlab.controller.form;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Tag {
	
	private String text;
	
	private Long weight;
	
	private String wholeWords;
	
	private String badWords;

}
