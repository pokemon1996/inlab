package com.example.duobot.inlab.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.duobot.inlab.constants.ResourceEndpoints;
import com.example.duobot.inlab.dto.EnterprisesDTORq;
import com.example.duobot.inlab.dto.EnterprisesDTORs;
import com.example.duobot.inlab.dto.EnterprisesDetailDTORs;
import com.example.duobot.inlab.service.EnterpriseService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(ResourceEndpoints.ENTERPRISES_API)
@RequiredArgsConstructor
public class EnterpriseController {

	private final EnterpriseService enterpriseService;
	
	@PostMapping
	public ResponseEntity<Object> createEnterprise(
		@RequestPart @Valid EnterprisesDTORq enterpriseBody,
		@RequestPart MultipartFile logo){
		
		try {
			this.enterpriseService.createEnterprise(enterpriseBody, logo);
			
			return ResponseEntity.status(HttpStatus.CREATED).build();
			
		} catch (Exception ex) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ex.getMessage());
		}
	}
	
	@PutMapping("/{enterprise_id}")
	public ResponseEntity<Object> editEnterprise(
		@PathVariable(name = ResourceEndpoints.ENTERPRISE_ID) UUID enterpriseId,
		@RequestPart @Valid EnterprisesDTORq enterpriseBody,
		@RequestPart(required = false) MultipartFile logo){
		
		try {
			
			enterpriseBody.setEnterpriseId(enterpriseId);
			
			this.enterpriseService.updateEnterprise(enterpriseBody, logo);
			
			return ResponseEntity.status(HttpStatus.CREATED).build();
			
		} catch (Exception ex) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ex.getMessage());
		}
	}
	
	@GetMapping(value = ResourceEndpoints.ENTERPRISE_DETAIL, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EnterprisesDetailDTORs> getDetailEnterprise(
		@PathVariable(value = ResourceEndpoints.ENTERPRISE_ID) UUID enterpriseId){
		
		return ResponseEntity.ok(this.enterpriseService.getDetailEnterprise(enterpriseId));
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<EnterprisesDTORs>> findAllEnterprises(){
		return ResponseEntity.ok(this.enterpriseService.findAllEnterprises());
	}
}
