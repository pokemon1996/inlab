package com.example.duobot.inlab.controller;

import java.io.File;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.duobot.inlab.constants.ResourceEndpoints;
import com.example.duobot.inlab.controller.form.ConferenceForm;
import com.example.duobot.inlab.dao.CampaignRepository;
import com.example.duobot.inlab.dao.ConferenceService;
import com.example.duobot.inlab.dao.GalleryRepository;
import com.example.duobot.inlab.dto.CampaignDTORq;
import com.example.duobot.inlab.model.Campaign;
import com.example.duobot.inlab.model.Conference;
import com.example.duobot.inlab.model.Gallery;
import com.example.duobot.inlab.service.CampaignService;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequiredArgsConstructor
public class CampaignRestController {

	private final CampaignRepository campaignRepository;
	
	private final GalleryRepository galleryService;

	private final ConferenceService conferenceService;

	private final CampaignService campaignService;
	
	@GetMapping(value = "/api/campaign")
	public ResponseEntity<?> getAllCampaigns(Authentication user) {
		try {
			return ResponseEntity.ok(this.campaignService.findAllCampaigns(user));
		} catch (Exception ex) {
			log.error("Error was generated getting all campaigns {}", ex.getMessage());
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}

	@PostMapping(value = "/api/campaign")
	public ResponseEntity<Object> addCampaign(Principal user, @RequestBody @Valid CampaignDTORq campaign) {
		try {
			this.campaignService.saveCampaign(campaign);
			
			return ResponseEntity.noContent().build();
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}

	@PutMapping(value = "/api/campaign/{campaignId}")
	public ResponseEntity<?> editCampaign(
		@PathVariable("campaignId") Integer campaignId,
		@RequestBody @Valid CampaignDTORq campaign) {

		try {
			campaign.setCampaignId(campaignId);
			
			this.campaignService.updateCampaign(campaign);
			
			return ResponseEntity.noContent().build();
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}

	@DeleteMapping(value = "/api/campaign")
	public ResponseEntity<?> deleteCampaign(@RequestParam Integer campaignId) {

		try {
			Campaign dbCampaign = campaignRepository.findById(campaignId).get();
			if (dbCampaign == null) {
				return ResponseEntity.badRequest().body("No encontramos este registro en el base de datos");
			}
			campaignRepository.delete(dbCampaign);
			return ResponseEntity.noContent().build();
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}
	
	@PostMapping(value = "/api/campaign/image")
	public ResponseEntity<?> addCampaignImage( @RequestParam("file") MultipartFile file, @RequestParam("campaignId") Integer campaignId) {


		if (file.isEmpty()) {
			return ResponseEntity.badRequest().body("Empty file");
		}

		try {
			Campaign dbCampaign = campaignRepository.findById(campaignId).get();
			if(dbCampaign == null) {
				return ResponseEntity.badRequest().body("No encontramos este registro en el base de datos");
			}
			File dir = new File("c:\\inlab\\campaign\\");
			if(!dir.exists()) {
				dir.mkdirs();
			}
			File finalDestination = new File(dir.getAbsolutePath() + "\\" + campaignId + "-" + file.getOriginalFilename());
			file.transferTo(finalDestination);
			dbCampaign.setPollImageUrl("/images/" + campaignId + "-" + file.getOriginalFilename());
			campaignRepository.save(dbCampaign);
			return ResponseEntity.noContent().build();
		} catch (Exception ex) {
			System.out.println(ex);
			return ResponseEntity.badRequest().body("Error agregando un archivo");
		}
	}

	@PostMapping(value = "/api/campaign/conference")
	public ResponseEntity<?> addCampaignConference( @RequestBody ConferenceForm form) {


		if (form == null) {
			return ResponseEntity.badRequest().body("Empty form");
		}

		try {
			Campaign dbCampaign = campaignRepository.findById(form.getCampaignId()).get();
			if(dbCampaign == null) {
				return ResponseEntity.badRequest().body("No encontramos este registro en el base de datos");
			}
			
			if(dbCampaign.getConferences() == null) {
				List<Conference> conferences = new ArrayList<Conference>();
				dbCampaign.setConferences(conferences);
			}
			Conference conf = new Conference();
			conf.setConferenceName(form.getConferenceName());
			conf.setConferenceUrl(form.getConferenceUrl());
			conf.setCampaign(dbCampaign);
			dbCampaign.getConferences().add(conf);
			campaignRepository.save(dbCampaign);
			conferenceService.save(conf);
			return ResponseEntity.noContent().build();
		} catch (Exception ex) {
			System.out.println(ex);
			return ResponseEntity.badRequest().body("Error agregando una conferencia");
		}
	}

	@DeleteMapping(value = "/api/campaign/conference")
	public ResponseEntity<?> deleteConference(@RequestParam int conferenceId) {
		try {
			Conference dbConference = conferenceService.findById(conferenceId).get();
			if(dbConference == null) {
				return ResponseEntity.badRequest().body("No encontramos este registro en el base de datos");
			}
			conferenceService.delete(dbConference);
			return ResponseEntity.noContent().build();
		} catch (Exception ex) {
			System.out.println(ex);
			return ResponseEntity.badRequest().body("Error borrando una conferencia");
		}
	}
	
	@PostMapping(value = "/api/campaign/galeria")
	public ResponseEntity<?> addGalleryImage( @RequestParam("file") MultipartFile file, @RequestParam("campaignId") Integer campaignId) {


		if (file.isEmpty()) {
			return ResponseEntity.badRequest().body("Empty file");
		}

		try {
			Campaign dbCampaign = campaignRepository.findById(campaignId).get();
			if(dbCampaign == null) {
				return ResponseEntity.badRequest().body("No encontramos este registro en el base de datos");
			}
			File dir = new File("c:\\inlab\\campaign\\");
			if(!dir.exists()) {
				dir.mkdirs();
			}
			Gallery gallery = new Gallery();
			gallery.setCampaign(dbCampaign);
			gallery = galleryService.save(gallery);
			String filePath = dir.getAbsolutePath() + "\\" + campaignId + "-" +gallery.getGalleryId() + "-" + file.getOriginalFilename();
			File finalDestination = new File(filePath);
			file.transferTo(finalDestination);
			gallery.setImage("/images/" + campaignId + "-" +gallery.getGalleryId() + "-" + file.getOriginalFilename());
			galleryService.save(gallery);
			dbCampaign.getGalleries().add(gallery);
			campaignRepository.save(dbCampaign);
			return ResponseEntity.noContent().build();
		} catch (Exception ex) {
			System.out.println(ex);
			return ResponseEntity.badRequest().body("Error agregando un archivo");
		}
	}
	
	@DeleteMapping(value = "/api/campaign/galeria")
	public ResponseEntity<?> deleteGalleryImage( @RequestParam Integer galleryId) {
		try {
			Gallery dbGallery = galleryService.findById(galleryId).get();
			if(dbGallery == null) {
				return ResponseEntity.badRequest().body("No encontramos este registro en el base de datos");
			}
			

			galleryService.delete(dbGallery);
			return ResponseEntity.noContent().build();
		} catch (Exception ex) {
			System.out.println(ex);
			return ResponseEntity.badRequest().body("Error borrando un archivo");
		}
	}

	@GetMapping(value = "/api/campaign/detail/edition/{campaign_id}")
	public ResponseEntity<Object> getCampaignDetailEdition(
		@PathVariable(name = ResourceEndpoints.CAMPAIGN_ID) Integer campaignId) {
		try {
			return ResponseEntity.ok(this.campaignService.getCampaignDetailEdition(campaignId));
		} catch (Exception ex) {
			log.error("Error was generated getting the detail campaign {}", ex.getMessage());
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}
	
	@GetMapping(value = ResourceEndpoints.CAMPAIGNS_BY_TYPE_AND_USER, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getCampaignsByCampaignType(
		@PathVariable(name = ResourceEndpoints.CAMPAIGN_ID_TYPE) UUID campaignTypeId,
		@PathVariable(name = ResourceEndpoints.USERNAME) String username) {

		return ResponseEntity.ok(this.campaignService.getCampaignsByType(campaignTypeId, username));
	}
	

}
