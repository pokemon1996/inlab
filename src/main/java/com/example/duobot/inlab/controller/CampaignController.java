package com.example.duobot.inlab.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.duobot.inlab.constants.ResourceEndpoints;
import com.example.duobot.inlab.service.CampaignService;

import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
public class CampaignController {

	private final CampaignService campaignService;

	@RequestMapping(value = "/detalleCampana/{campaign_id}")
	public String detalleCampana(
		@PathVariable(name = ResourceEndpoints.CAMPAIGN_ID) Integer campaignId, 
		HttpServletRequest request, Authentication user, Model model) {

		request.getSession().setAttribute("campaign", this.campaignService.getCampaignDetailClientView(campaignId));
		
		return "detalleCampana";
	}
}
