package com.example.duobot.inlab.constants;

public final class CommonConstants {

	private CommonConstants() {}
	
	public static final String DEFAULT_BACKGROUND_COLOR_INLAB = "F19300";
	
	public static final String DEFAULT_TITLE_COLOR_INLAB = "9A9A9A";
	
	public static final String SHARP_CHARACTER = "#";
	
	public static final String SLASH_CHARACTER = "/";
	
	public static final String DD_MM_YYYY_SLASH = "dd/MM/yyyy";
}
