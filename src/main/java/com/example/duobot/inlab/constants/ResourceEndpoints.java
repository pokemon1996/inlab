package com.example.duobot.inlab.constants;

public final class ResourceEndpoints {

	private ResourceEndpoints() {}
	
	public static final String API_V1 = "/api/v1";
	public static final String ENTERPRISES_API = API_V1 + "/enterprises";
	public static final String CAMPAIGN_TYPE_API = API_V1 + "/campaign-type";
	public static final String RESOURCE_TYPE_API = API_V1 + "/resource-type";
	public static final String RESOURCE_CAMPAIGN_API = API_V1 + "/resource-campaign";
	public static final String GALLERIES_API = API_V1 + "/galleries";
	public static final String RESOURCES_BY_CAMPAIGN = "/campaign/{campaign_id}";
	public static final String RESOURCES_BY_CAMPAIGN_TYPE_AND_USER = "/campaign-type/{campaing_id_type}/user/{username}";
	public static final String GALLERIES_BY_CAMPAIGN = "/campaign/{campaign_id}";
	public static final String GALLERIES_BY_CAMPAIGN_TYPE_AND_USER = "/campaign-type/{campaing_id_type}/user/{username}";
	public static final String ENTERPRISE_DETAIL = "/detail/{enterprise_id}";
	public static final String CAMPAIGNS_BY_TYPE_AND_USER = API_V1 + "/campaigns/type/{campaing_id_type}/user/{username}";
	public static final String ANSWER_POLL_BY_CAMPAIGN = API_V1 + "/answer/campaign/{campaign_id}";
	public static final String ANSWER_POLL_BY_CAMPAIGN_TYPE_AND_USER = API_V1 + "/answer/campaign-type/{campaing_id_type}/user/{username}";
	
	public static final String CAMPAIGN_ID_TYPE = "campaing_id_type";
	public static final String USERNAME = "username";
	public static final String ENTERPRISE_ID = "enterprise_id";
	public static final String CAMPAIGN_ID = "campaign_id";
}
