package com.example.duobot.inlab.dto;

import java.io.Serializable;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResourceCampaignDTORq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6323529486676254164L;

	private String resourceUrl;
	
	private UUID resourceTypeId;
}
