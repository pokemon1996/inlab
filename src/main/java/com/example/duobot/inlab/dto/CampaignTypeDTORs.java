package com.example.duobot.inlab.dto;

import java.io.Serializable;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CampaignTypeDTORs implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2366243820548242833L;

	private UUID campaingIdType;
	
	private String name;
	
	@JsonIgnore
	private String fileNameLogo;
	
	private String backgroundImageUrl;
}
