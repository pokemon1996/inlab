package com.example.duobot.inlab.dto;

import java.io.Serializable;
import java.util.UUID;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor            
public class EnterprisesDTORq implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7333590531291320947L;

	@JsonIgnore
	private UUID enterpriseId;
	
	@NotBlank(message = "Identification can't be null/empty")
	private String identification;
	
	private Integer dv;
	
	private String businessName;
	
	private String backgroundColorHex;
	
	private String titleColorHex;

}
