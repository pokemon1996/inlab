package com.example.duobot.inlab.dto;

import java.io.Serializable;
import java.util.UUID;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserCreationDTORq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1395597430677283318L;

	@JsonIgnore
	private Integer userId;
	
	@Email
	@NotBlank
	private String username;
	
	@NotNull
	private String completeName;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	@NotNull
	private String password;
	
	@NotNull
	private String roleName;
	
	private UUID enterpriseId;
}
