package com.example.duobot.inlab.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserListDTORs implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9162307058232860245L;

	private Integer userId;
	
	private String username;

	private String completeName;

	private String roleName;
	
	private boolean enabled;
	
	private EnterprisesDTORs enterprise;
}
