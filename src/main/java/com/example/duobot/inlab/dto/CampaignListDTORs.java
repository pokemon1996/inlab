package com.example.duobot.inlab.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import com.example.duobot.inlab.constants.CommonConstants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = Include.NON_NULL)
public class CampaignListDTORs implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8784248633986824510L;

	private Integer campaignId;

	private String campaignName;
		
	private String campaignCustomer;
	
	private String campaignProject;

	private Integer startDate;

	private Integer endDate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = CommonConstants.DD_MM_YYYY_SLASH)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate startCampaignDate;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = CommonConstants.DD_MM_YYYY_SLASH)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate endCampaignDate;
	
	private String videoUrl;

	private String chatUrl;

	private String pollUrl;

	private Integer insightId;

	private String powerBIUrl;

	private String timelineUrl;

	private String tweetsUrl;

	private String topicsUrl;

	private String influenceUrl;

	private String topDomainsUrl;

	private String countries;

	private String demographics;

	private String postType;
	
	private String sources;

	private String assignedUser;
	
	private String pollImageUrl;
	
	private String emotionsUrl;

	private String emotionsIA;
	
	private List<GalleryDTORs> galleries;

	private List<ConferenceDTORs> conferences;

	private CampaignTypeDTORs campaignType;

	private List<ResourceCampaignDTORs> resourceCampaigns;
	
	private EnterprisesDTORs enterprise;

}
