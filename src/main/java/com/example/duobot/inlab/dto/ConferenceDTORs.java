package com.example.duobot.inlab.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ConferenceDTORs implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3209580696942092153L;

	private int conferenceId;
	
    private String conferenceName;
    
    private String conferenceUrl;
}
