package com.example.duobot.inlab.dto.client.dashboard;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GalleryDTORs implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -211134541495732235L;

	private String image;
}
