package com.example.duobot.inlab.dto.session;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTOSessionRs implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4307305690266663666L;

	private String username;
	
	private String completeName;

	private EnterpriseDTOSessionRs enterprise;
	
}
