package com.example.duobot.inlab.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor  
public class EnterprisesDetailDTORs implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1169429994535661539L;
	
	private String identification;
	
	private String businessName;
	
	private String backgroundColorHex;
	
	private String titleColorHex;

	private String pathLogo; 

}
