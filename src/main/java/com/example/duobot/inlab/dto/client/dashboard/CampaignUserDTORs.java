package com.example.duobot.inlab.dto.client.dashboard;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CampaignUserDTORs implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4696659838280878342L;

	private String campaignName;
	
	private String campaignCustomer;
	
	private String campaignProject;
	
	private List<GalleryDTORs> galleries;
	
	private List<ConferenceDTORs> conferences;
	
	private Map<String,List<CampaignResourceDTORs>> resourceCampaignsMap;
}
