package com.example.duobot.inlab.dto.session;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EnterpriseDTOSessionRs implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4703467813949858402L;

	private String identification;
		
	private String businessName;
	
	private String backgroundColorHex;
	
	private String titleColorHex;
	
	private String logo;
	
	private String contentTypeLogo;
}
