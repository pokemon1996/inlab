package com.example.duobot.inlab.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GalleryDTORs implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8752823866175995472L;

	private Integer galleryId;
	
	private String image;
}
