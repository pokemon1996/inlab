package com.example.duobot.inlab.dto.client.dashboard;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ConferenceDTORs implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -469975112877110639L;

    private String conferenceName;
    
    private String conferenceUrl;
}
