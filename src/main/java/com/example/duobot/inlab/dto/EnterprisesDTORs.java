package com.example.duobot.inlab.dto;

import java.io.Serializable;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor  
public class EnterprisesDTORs implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2428123383958076753L;

	private UUID enterpriseId;
		
	private String identification;

	private String businessName;
	
	private String backgroundColorHex;
	
	private String titleColorHex;

	private String pathLogo;
}
