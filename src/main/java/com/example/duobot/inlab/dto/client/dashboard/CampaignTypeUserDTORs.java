package com.example.duobot.inlab.dto.client.dashboard;

import java.io.Serializable;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CampaignTypeUserDTORs implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1517645618167697061L;

	private UUID campaingIdType;

	private Boolean isActive;
	
	private String name;
	
	private String description;

	private String backgroundImageUrl;
	
}
