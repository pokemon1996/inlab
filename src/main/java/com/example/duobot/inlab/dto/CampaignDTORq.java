package com.example.duobot.inlab.dto;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.example.duobot.inlab.constants.CommonConstants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CampaignDTORq {

	@JsonIgnore
	private Integer campaignId;
	
	private String campaignName;
	
	private String campaignCustomer;
	
	private String campaignProject;

	@NotBlank(message = "Campaña debe tener usuario asignado")
	private String assignedUser;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = CommonConstants.DD_MM_YYYY_SLASH)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate startCampaignDate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = CommonConstants.DD_MM_YYYY_SLASH)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate endCampaignDate;

	@NotNull(message = "Please specify a campaign type")
	private UUID campaingIdType;

	@OneToMany()
	private List<ResourceCampaignDTORq> resourceCampaigns;
	
	private Integer insightId;
	
}
