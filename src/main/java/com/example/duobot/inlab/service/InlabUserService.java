package com.example.duobot.inlab.service;

import com.example.duobot.inlab.dto.session.UserDTOSessionRs;
import com.example.duobot.inlab.model.User;

public interface InlabUserService {

	User findByUsername(String username);

	UserDTOSessionRs getUserSession(String userName);
	
}
