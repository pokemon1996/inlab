package com.example.duobot.inlab.service.impl;

import java.text.DateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.duobot.inlab.dao.CampaignRepository;
import com.example.duobot.inlab.dao.CampaignTypeRepository;
import com.example.duobot.inlab.dao.InlabUserRepository;
import com.example.duobot.inlab.dao.PollRepository;
import com.example.duobot.inlab.dao.ResourceCampaignRepository;
import com.example.duobot.inlab.dao.ResourceTypeRepository;
import com.example.duobot.inlab.dto.CampaignDTORq;
import com.example.duobot.inlab.dto.CampaignListDTORs;
import com.example.duobot.inlab.dto.CampaignTypeDTORs;
import com.example.duobot.inlab.dto.EnterprisesDTORs;
import com.example.duobot.inlab.dto.ResourceCampaignDTORq;
import com.example.duobot.inlab.mapper.CampaignMapper;
import com.example.duobot.inlab.model.Campaign;
import com.example.duobot.inlab.model.CampaignType;
import com.example.duobot.inlab.model.Enterprises;
import com.example.duobot.inlab.model.ResourceCampaign;
import com.example.duobot.inlab.model.ResourceType;
import com.example.duobot.inlab.model.User;
import com.example.duobot.inlab.service.CampaignService;
import com.example.duobot.inlab.service.CampaignTypeService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CampaignServiceImpl implements CampaignService{


	
	private final CampaignTypeService campaignTypeService;
	
	private final CampaignMapper campaignMapper;
	
	private final CampaignRepository campaignRepository;
	
	private final CampaignTypeRepository campaignTypeRepository;
	
	private final ResourceTypeRepository resourceTypeRepository;
	
	private final ResourceCampaignRepository resourceCampaignRepository;
	
	private final InlabUserRepository inlabUserRepository;
		
	@Transactional
	public void saveCampaign(final CampaignDTORq campaignDTORq) {

		final Campaign newCampaign = this.campaignMapper.mapDtoToEntityCreation(campaignDTORq);
		
		final CampaignType campaignType = this.campaignTypeRepository.findById(campaignDTORq.getCampaingIdType())
			.orElseThrow(() -> new IllegalArgumentException("Campaign type not found"));

		newCampaign.setCampaignType(campaignType);
		
		final Enterprises enterprises = Optional.ofNullable(this.inlabUserRepository.findByUsername(campaignDTORq.getAssignedUser()))
			.map(User::getEnterprise)
			.orElseThrow(() -> new IllegalArgumentException("Enterprise not found"));
		
		newCampaign.setCampaignCustomer(enterprises.getBusinessName());
		
		final LocalDateTime currentDate = LocalDateTime.now();
		
		newCampaign.setCreationDate(currentDate);
		newCampaign.setModificationDate(currentDate);
		
		this.campaignRepository.save(newCampaign);
		
		this.saveCampaignResources(campaignDTORq.getResourceCampaigns(), newCampaign);
	}
	
	@Transactional
	public void updateCampaign(final CampaignDTORq campaignDTORq) {
		
		final Campaign currentCampaign = this.campaignRepository.findById(campaignDTORq.getCampaignId())
			.orElseThrow(() -> new IllegalArgumentException("Campaign not found"));
		
		this.campaignMapper.mapDtoToEntityUpdate(campaignDTORq, currentCampaign);
		
		final CampaignType campaignType = this.campaignTypeRepository.findById(campaignDTORq.getCampaingIdType())
			.orElseThrow(() -> new IllegalArgumentException("Campaign type not found"));

		currentCampaign.setCampaignType(campaignType);
				
		final Enterprises enterprises = Optional.ofNullable(this.inlabUserRepository.findByUsername(campaignDTORq.getAssignedUser()))
			.map(User::getEnterprise)
			.orElseThrow(() -> new IllegalArgumentException("Enterprise not found"));
			
		currentCampaign.setCampaignCustomer(enterprises.getBusinessName());
			
		currentCampaign.setModificationDate(LocalDateTime.now());
		
		this.campaignRepository.save(currentCampaign);

		this.resourceCampaignRepository.deleteAllByCampaign(campaignDTORq.getCampaignId());
		
		this.saveCampaignResources(campaignDTORq.getResourceCampaigns(), currentCampaign);
	}
	
	private void saveCampaignResources(
		final List<ResourceCampaignDTORq> campaignResources,
		final Campaign currentCampaign) {
		
		final Map<UUID, ResourceType> mapResourceTypeRequested = new HashMap<>();

		Optional.ofNullable(campaignResources)
			.ifPresent(resources -> resources
				.forEach(resource -> {
					
					final ResourceType resourceTypeSelected = Optional.ofNullable(resource.getResourceTypeId())
						.filter(mapResourceTypeRequested::containsKey)
						.map(mapResourceTypeRequested::get)
						.orElseGet(() -> this.resourceTypeRepository.findById(resource.getResourceTypeId())
							.map(typeRequested -> {
								mapResourceTypeRequested.putIfAbsent(typeRequested.getResourceTypeId(), typeRequested);
								
								return typeRequested;
							})
							.orElseThrow(() -> new IllegalArgumentException("Resource type not found")));
					
					final ResourceCampaign resourceCampaign = ResourceCampaign.builder()
						.resourceCampaignId(UUID.randomUUID())
						.campaign(currentCampaign)
						.resourceType(resourceTypeSelected)
						.resourceUrl(resource.getResourceUrl())
						.creationDate(LocalDateTime.now(ZoneOffset.UTC))
						.modificationDate(LocalDateTime.now(ZoneOffset.UTC))
						.build();
					
					this.resourceCampaignRepository.save(resourceCampaign);	
				})
			);
	}
	
	public List<CampaignListDTORs> findAllCampaigns(Authentication user) {
		
		String authority = null;
		
		final List<Campaign> campaigns = new ArrayList<>();
		
		for (GrantedAuthority auth : user.getAuthorities()) {
			authority = auth.getAuthority();
			break;
		}
		
		if (authority.equals("Admin")) {
				
			campaigns.addAll(campaignRepository.findAll());
			
		} else {
			
			Long timestamp = new Date().getTime();
			timestamp = timestamp / 1000;
			
			campaigns.addAll(campaignRepository.findByAssignedUserAndEndDateGreaterThanOrderByStartDateDesc(
					user.getName(), timestamp.intValue()));
		}
		
		return Optional.of(campaigns)
			.map(campaignList -> campaignList.stream()
				.map(this.campaignMapper::mapListCampaignsTODTO)
				.collect(Collectors.toList()))
			.orElseGet(Collections::emptyList);
	}
	
	public CampaignListDTORs getCampaignDetailEdition(final Integer campaignId) {
		return this.campaignRepository.findById(campaignId)
			.map(this.campaignMapper::mapCampaignDetailEditToDTO)
			.map(campaign -> {
				
				Optional.ofNullable(this.inlabUserRepository.findByUsername(campaign.getAssignedUser()))
					.map(User::getEnterprise)
					.ifPresent(enterprise -> {
						campaign.setEnterprise(EnterprisesDTORs
							.builder()
							.enterpriseId(enterprise.getEnterpriseId())
							.businessName(enterprise.getBusinessName())
							.build());
					});
				
				return campaign;
			})
			.orElseThrow(() -> new IllegalArgumentException("Campaign not found"));
	}
	
	public List<CampaignListDTORs> getCampaignsByType(UUID campaignTypeId, String userName){
		final CampaignType campaignType = CampaignType.builder()
			.campaingIdType(campaignTypeId)
			.build();
		
		return Optional.ofNullable(this.campaignRepository
				.findByCampaignTypeAndAssignedUserOrderByStartCampaignDateDesc(campaignType, userName))
			.map(campaigns -> campaigns.stream()
				.map(this.campaignMapper::mapCampaignDetailEditToDTO)
				.collect(Collectors.toList()))
			.orElseGet(Collections::emptyList);
	}
	
	public CampaignListDTORs getCampaignDetailClientView(final Integer campaignId) {
		return this.campaignRepository.findById(campaignId)
			.map(this.campaignMapper::mapCampaignDetailClientViewToDTO)
			.map(campaign -> {
				
				Optional.ofNullable(campaign.getCampaignType())
					.map(CampaignTypeDTORs::getFileNameLogo)
					.ifPresent(fileNameLogo -> {
						campaign.getCampaignType().setBackgroundImageUrl(this.campaignTypeService.getBackgroundImageUrl(fileNameLogo));
					});

				return campaign;
			})
			.orElseThrow(() -> new IllegalArgumentException("Campaign not found"));
	}
}
