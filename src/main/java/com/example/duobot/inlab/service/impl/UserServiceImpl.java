package com.example.duobot.inlab.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.duobot.inlab.dao.EnterprisesRepository;
import com.example.duobot.inlab.dao.InlabUserRepository;
import com.example.duobot.inlab.dto.UserCreationDTORq;
import com.example.duobot.inlab.dto.UserListDTORs;
import com.example.duobot.inlab.mapper.UserMapper;
import com.example.duobot.inlab.model.Enterprises;
import com.example.duobot.inlab.model.User;
import com.example.duobot.inlab.service.UserService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService, UserDetailsService {
	
	private final UserMapper userMapper;
	
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	
	private final InlabUserRepository userRepository;

	private final EnterprisesRepository enterprisesRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return this.userRepository.findByUsername(username);
	}

	public void createUser(UserCreationDTORq creationDTORq) {
		Optional.of(creationDTORq.getUsername())
			.filter(userName -> Objects.isNull(this.userRepository.findByUsername(userName)))
			.map(userName -> {
				
				final Enterprises enterprise = Optional.ofNullable(creationDTORq.getEnterpriseId())
					.map(enterpriseId -> this.enterprisesRepository.findById(enterpriseId)
						.orElseThrow(() -> new IllegalArgumentException("Enterprise not found")))
					.orElse(null);
					
				final User user = this.userMapper.mapCreationUserTOEntity(creationDTORq);
				
				user.setEnabled(true);
				user.setEnterprise(enterprise);
				user.setPassword(this.bCryptPasswordEncoder.encode(user.getPassword()));
				
				return this.userRepository.save(user);
			})
			.orElseThrow(() -> new IllegalArgumentException("Ya existe un usuario con este email"));
	}
	
	public void updateUser(UserCreationDTORq creationDTORq) {
		
		this.userRepository.findById(creationDTORq.getUserId())
			.map(entity -> {
				
				if (!entity.getUsername().equals(creationDTORq.getUsername())) {
					Optional.ofNullable(this.userRepository.findByUsername(creationDTORq.getUsername()))
						.ifPresent(userDifferent -> {
							throw new IllegalArgumentException("Ya existe un usuario con este email");
						});
					
				}
				
				final Enterprises enterprise = Optional.ofNullable(creationDTORq.getEnterpriseId())
						.map(enterpriseId -> this.enterprisesRepository.findById(enterpriseId)
							.orElseThrow(() -> new IllegalArgumentException("Enterprise not found")))
						.orElse(null);
				
				final User user = this.userMapper.mapModificationUserTOEntity(creationDTORq, entity);
				
				user.setEnabled(true);
				user.setEnterprise(enterprise);
				user.setPassword(this.bCryptPasswordEncoder.encode(user.getPassword()));
				
				return this.userRepository.save(user);
			})
			.orElseThrow(() -> new IllegalArgumentException("Usuario no encontrado"));
	}
	
	public List<UserListDTORs> getListUsers() {
		return StreamSupport.stream(this.userRepository.findAll().spliterator(), false)
			.map(this.userMapper::mapUserTOUserDTOList)
			.collect(Collectors.toList());
	}
	
	public List<UserListDTORs> findByEnterprise(UUID enterpriseId) {
		final Enterprises enterprises = Enterprises.builder()
			.enterpriseId(enterpriseId)
			.build();
		
		return Optional.ofNullable(this.userRepository.findByEnterprise(enterprises))
			.map(users -> users.stream()
				.map(this.userMapper::mapUserTOUserDTOKeyValue)
				.collect(Collectors.toList()))
			.orElseGet(Collections::emptyList);			
	}
}