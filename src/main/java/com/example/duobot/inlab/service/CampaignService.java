package com.example.duobot.inlab.service;

import java.util.List;
import java.util.UUID;

import org.springframework.security.core.Authentication;

import com.example.duobot.inlab.dto.CampaignDTORq;
import com.example.duobot.inlab.dto.CampaignListDTORs;

public interface CampaignService {

	void saveCampaign(CampaignDTORq campaignDTORq);
	
	void updateCampaign(CampaignDTORq campaignDTORq);
	
	List<CampaignListDTORs> findAllCampaigns(Authentication user);
	
	CampaignListDTORs getCampaignDetailEdition(Integer campaignId);
	
	List<CampaignListDTORs> getCampaignsByType(UUID campaignTypeId, String userName);
	
	CampaignListDTORs getCampaignDetailClientView(Integer campaignId);

}
