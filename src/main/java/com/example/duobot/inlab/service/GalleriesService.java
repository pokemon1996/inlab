package com.example.duobot.inlab.service;

import java.util.List;
import java.util.UUID;

import com.example.duobot.inlab.dto.GalleryDTORs;

public interface GalleriesService {

	List<GalleryDTORs> getGalleriesByCampaign(Integer campaignId);
	
	List<GalleryDTORs> getGalleriesByCampaignTypeAndUserName(UUID campaingIdType, String username);
}
