package com.example.duobot.inlab.service.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.example.duobot.inlab.dao.ResourceCampaignRepository;
import com.example.duobot.inlab.dto.client.dashboard.CampaignResourceDTORs;
import com.example.duobot.inlab.mapper.ResourceCampaignMapper;
import com.example.duobot.inlab.model.Campaign;
import com.example.duobot.inlab.service.ResourceCampaignService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ResourceCampaignServiceImpl implements ResourceCampaignService{

	private final ResourceCampaignMapper resourceCampaignMapper;

	private final ResourceCampaignRepository resourceCampaignRepository;
	
	public Map<String, List<CampaignResourceDTORs>> getResourcesCampaignByCampaign(Integer campaignId){
		final Campaign campaign = Campaign.builder()
			.campaignId(campaignId)
			.build();
		
		return this.resourceCampaignMapper.mapRersoucesCampaignAsMapCollect(
			this.resourceCampaignRepository.findByCampaign(campaign));
	}
	
	public Map<String, List<CampaignResourceDTORs>> getResourcesCampaignByCampaignTypeUserAssigned(UUID campaignTypeId, String username){
		return this.resourceCampaignMapper.mapRersoucesCampaignAsMapCollect(
			this.resourceCampaignRepository.findByCampaignTypeAndUserName(campaignTypeId, username));
	}
}
