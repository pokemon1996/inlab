package com.example.duobot.inlab.service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.example.duobot.inlab.dto.client.dashboard.CampaignResourceDTORs;

public interface ResourceCampaignService {

	Map<String, List<CampaignResourceDTORs>> getResourcesCampaignByCampaign(Integer campaignId);
	
	Map<String, List<CampaignResourceDTORs>> getResourcesCampaignByCampaignTypeUserAssigned(UUID campaignTypeId, String username);

}