package com.example.duobot.inlab.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;

import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.duobot.inlab.constants.CommonConstants;
import com.example.duobot.inlab.dao.InlabUserRepository;
import com.example.duobot.inlab.dto.session.UserDTOSessionRs;
import com.example.duobot.inlab.mapper.UserMapper;
import com.example.duobot.inlab.model.Enterprises;
import com.example.duobot.inlab.model.User;
import com.example.duobot.inlab.service.InlabUserService;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@RequiredArgsConstructor
public class InlabUserServiceImpl implements InlabUserService{

	@Value("${services.enterprises.resources}")
	private String pathLogos;

	@Value("${services.images.generic-handler.folder-name}")
	private String handlerImages;
	
	private final UserMapper userMapper;
	
	private final InlabUserRepository inlabUserRepository;

	/**
	 * TODO Required a refactor
	 */
	@Override
	public User findByUsername(String username) {
		return this.inlabUserRepository.findByUsername(username);
	}
	
	public UserDTOSessionRs getUserSession(String userName) {
		
		final User user = Optional.ofNullable(this.inlabUserRepository.findByUsername(userName))
			.orElseThrow(() -> new IllegalArgumentException("User not found"));
		
		final UserDTOSessionRs dtoSessionRs = this.userMapper.mapEntityToDTO(user);
		
		Optional.ofNullable(user.getEnterprise())
			.map(Enterprises::getEnterpriseLayout)
			.ifPresent(enterpriseLayout -> {
				
				dtoSessionRs.getEnterprise()
					.setBackgroundColorHex(Hex.encodeHexString(enterpriseLayout.getBackgroundColor()));
			
				dtoSessionRs.getEnterprise()
					.setTitleColorHex(Hex.encodeHexString(enterpriseLayout.getTitleColor()));
			
				final File pathEnterprise = new File(pathLogos, user.getEnterprise().getEnterpriseId().toString());
				
				final File logoEnterprise = new File(pathEnterprise, user.getEnterprise().getEnterpriseLayout().getFileNameLogo());

				Optional.of(logoEnterprise)
					.filter(File::isFile)
					.ifPresent(logo -> {
						try {
							String contentType = Files.probeContentType(logo.toPath());
									
							dtoSessionRs.getEnterprise().setLogo(ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString() + 
								handlerImages + 
								user.getEnterprise().getEnterpriseId().toString() +
								CommonConstants.SLASH_CHARACTER +
								logoEnterprise.getName());
							dtoSessionRs.getEnterprise().setContentTypeLogo(contentType);
							
						} catch (IOException e) {
							log.error("Unable to get the enterprise logo");
						}
					});
			});
		
		return dtoSessionRs;
	}
}
