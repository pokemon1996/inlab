package com.example.duobot.inlab.service.impl;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.example.duobot.inlab.dao.GalleryRepository;
import com.example.duobot.inlab.dto.GalleryDTORs;
import com.example.duobot.inlab.mapper.GalleryMapper;
import com.example.duobot.inlab.service.GalleriesService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class GalleriesServiceImpl implements GalleriesService{

	private final GalleryRepository galleryRepository;
	
	private final GalleryMapper galleryMapper;
	
	public List<GalleryDTORs> getGalleriesByCampaign(final Integer campaignId){
		return this.galleryRepository.findByCampaignCampaignId(campaignId)
			.stream()
			.map(this.galleryMapper::mapEntityTODTO)
			.collect(Collectors.toList());
	}
	
	public List<GalleryDTORs> getGalleriesByCampaignTypeAndUserName(UUID campaingIdType, String username){
		return this.galleryRepository.findByCampaignTypeAndUserName(campaingIdType, username)
			.stream()
			.map(this.galleryMapper::mapEntityTODTO)
			.collect(Collectors.toList());
	}
}
