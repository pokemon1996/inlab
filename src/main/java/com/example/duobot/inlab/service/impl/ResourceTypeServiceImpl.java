package com.example.duobot.inlab.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.example.duobot.inlab.dao.ResourceTypeRepository;
import com.example.duobot.inlab.dto.ResourceTypeDTORs;
import com.example.duobot.inlab.mapper.ResourceTypeMapper;
import com.example.duobot.inlab.service.ResourceTypeService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ResourceTypeServiceImpl implements ResourceTypeService{

	private final ResourceTypeMapper resourceTypeMapper;
	
	private final ResourceTypeRepository resourceTypeRepository;
	
	public List<ResourceTypeDTORs> findAllResourceTypes(){
		return this.resourceTypeRepository.findAll()
			.stream()
			.map(this.resourceTypeMapper::mapFindAllResourceType)
			.collect(Collectors.toList());
	}
}
