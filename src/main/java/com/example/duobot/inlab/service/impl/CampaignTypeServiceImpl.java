package com.example.duobot.inlab.service.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.duobot.inlab.dao.CampaignTypeRepository;
import com.example.duobot.inlab.dto.CampaignTypeDTORs;
import com.example.duobot.inlab.dto.client.dashboard.CampaignTypeUserDTORs;
import com.example.duobot.inlab.mapper.CampaignTypeMapper;
import com.example.duobot.inlab.service.CampaignTypeService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CampaignTypeServiceImpl implements CampaignTypeService {
	
	@Value("${services.campaign-type.resource-handler.folder-name}")
	public String handlerImages;
	
	private final CampaignTypeMapper campaignTypeMapper;
	
	private final CampaignTypeRepository campaignTypeRepository;
	
	public List<CampaignTypeDTORs> findAllCampaigns() {
		return this.campaignTypeRepository.findAll()
			.stream()
			.map(this.campaignTypeMapper::mapFindAllCampaign)
			.collect(Collectors.toList());
	}
	
	public List<CampaignTypeUserDTORs> findCampaignsTypeByUser(final String userAssigned){
		return Optional.ofNullable(this.campaignTypeRepository.findAllOrderByClientOrderAsc())
			.map(ls -> ls.stream()
				.map(campaignType -> {
					
					final CampaignTypeUserDTORs dtoRs = this.campaignTypeMapper.mapCampaignTypeByUser(campaignType); 

					dtoRs.setBackgroundImageUrl(this.getBackgroundImageUrl(campaignType.getFileNameLogo()));
					dtoRs.setIsActive(this.campaignTypeRepository.checkCampaignTypeByUser(userAssigned, campaignType.getCampaingIdType().toString()) == 1);
					
					return dtoRs;
				})
				.sorted(Comparator.comparing(CampaignTypeUserDTORs::getIsActive, Comparator.nullsLast(Comparator.reverseOrder())))
				.collect(Collectors.toList()))
			.orElseGet(Collections::emptyList);
	}
	
	public String getBackgroundImageUrl(String logo) {
		return ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString() + 
			handlerImages + 
			logo;
	}
}
