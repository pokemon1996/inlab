package com.example.duobot.inlab.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.multipart.MultipartFile;

import com.example.duobot.inlab.dao.EnterpriseLayoutRepository;
import com.example.duobot.inlab.dao.EnterprisesRepository;
import com.example.duobot.inlab.dto.EnterprisesDTORq;
import com.example.duobot.inlab.dto.EnterprisesDTORs;
import com.example.duobot.inlab.dto.EnterprisesDetailDTORs;
import com.example.duobot.inlab.mapper.EnterpriseMapper;
import com.example.duobot.inlab.model.EnterpriseLayout;
import com.example.duobot.inlab.model.Enterprises;
import com.example.duobot.inlab.service.EnterpriseService;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@RequiredArgsConstructor
public class EnterpriseServiceImpl implements EnterpriseService{

	@Value("${services.enterprises.resources}")
	private String pathLogos;
	
	private final EnterpriseMapper enterpriseMapper;
	
	private final EnterprisesRepository enterprisesRepository;
	
	private final EnterpriseLayoutRepository enterpriseLayoutRepository;
	
	@Override
	@Transactional
	public void createEnterprise(
		final EnterprisesDTORq enterprisesDTOIn,
		final MultipartFile logo) {

		this.enterprisesRepository.findByIdentification(enterprisesDTOIn.getIdentification())
			.ifPresent(enterprise -> {
				throw new IllegalArgumentException("Empresa ya existe");
			});
		
		final Enterprises newEnterprise = this.enterpriseMapper.mapDTOToEntityCreation(enterprisesDTOIn);

		final EnterpriseLayout newEnterpriseLayout = this.enterpriseMapper.mapEnterpriseLayoutTOEntityCreation(enterprisesDTOIn);

		this.processLogo(newEnterprise.getEnterpriseId(), logo)
			.ifPresent(logoName -> {
				newEnterpriseLayout.setFileNameLogo(logoName);
			});
		
		final Enterprises enterprisesGenerated = this.enterprisesRepository.save(newEnterprise);
		
		newEnterpriseLayout.setEnterprise(enterprisesGenerated);
		
		this.enterpriseLayoutRepository.save(newEnterpriseLayout);
	}
	
	@Override
	@Transactional
	public void updateEnterprise(
		final EnterprisesDTORq enterprisesDTOIn,
		final MultipartFile logo) {

		this.enterprisesRepository.findById(enterprisesDTOIn.getEnterpriseId())
			.map(enterprise -> {
				
				if (!enterprise.getIdentification().equals(enterprisesDTOIn.getIdentification())) {
					Optional.ofNullable(this.enterprisesRepository.findByIdentification(enterprisesDTOIn.getIdentification()))
						.ifPresent(userDifferent -> {
							throw new IllegalArgumentException("Ya existe una empresa con la misma identificación");
						});
				}
				
				final Enterprises enterpriseUpdated = this.enterpriseMapper.mapModificationEnterpriseTOEntity(enterprisesDTOIn, enterprise);
			
				enterpriseUpdated.setModificationDate(LocalDateTime.now(ZoneOffset.UTC));
				
				final EnterpriseLayout mergeEnterpriseLayout = this.enterpriseMapper.mapEnterpriseLayoutTOEntityCreation(enterprisesDTOIn);

				Optional.ofNullable(enterprise.getEnterpriseLayout())
					.ifPresent(layout -> {
						
						mergeEnterpriseLayout.setLayoutId(layout.getLayoutId());
						mergeEnterpriseLayout.setCreationDate(layout.getCreationDate());
						mergeEnterpriseLayout.setFileNameLogo(layout.getFileNameLogo());
						
						this.processLogo(enterpriseUpdated.getEnterpriseId(), logo)
							.ifPresent(logoName -> {
								mergeEnterpriseLayout.setFileNameLogo(logoName);
							});
					});
				

				final Enterprises enterprisesGenerated = this.enterprisesRepository.save(enterpriseUpdated);
				
				mergeEnterpriseLayout.setEnterprise(enterprisesGenerated);
				
				this.enterpriseLayoutRepository.save(mergeEnterpriseLayout);
				
				return enterprise;
			})
			.orElseThrow(() -> new IllegalArgumentException("Empresa no encontrada"));
	}
	
	private Optional<String> processLogo(final UUID enterpriseId, final MultipartFile logo) {
		return Optional.ofNullable(logo)
			.map(logoAux -> {
				
				final File pathEnterprise = new File(pathLogos, enterpriseId.toString());
				
				pathEnterprise.mkdir();
				
				final String extension = MimeTypeUtils.parseMimeType(logo.getContentType()).getSubtype();
					
				return Optional.of(pathEnterprise.exists())
					.filter(Boolean::booleanValue)
					.map(isCreatedPath -> {
						
						String fileLogoName = null;
						
						File fileLogo = new File(pathEnterprise, UUID.randomUUID().toString() + "-logo." + extension);
						
						try (InputStream logoStream = logo.getInputStream()) {
						    try {
								Files.copy(logoStream, Paths.get(fileLogo.toURI()), StandardCopyOption.REPLACE_EXISTING);
								
								fileLogoName = fileLogo.getName();
								
							} catch (IOException e1) {
								log.error("Unable to copy logo");
							}
						} catch (IOException e2) {
							log.error("Unable to get logo");
						}
						
						return fileLogoName;
					})
					.orElse(null);
			});
	}
	
	public List<EnterprisesDTORs> findAllEnterprises(){
		return this.enterprisesRepository.findAll()
			.stream()
			.map(this.enterpriseMapper::mapEntityToDTO)
			.collect(Collectors.toList());
	}
	
	public EnterprisesDetailDTORs getDetailEnterprise(final UUID enterpriseId) {
		return this.enterprisesRepository.findById(enterpriseId)
			.map(this.enterpriseMapper::mapEnterpriseToEnterpriseDetailDTO)
			.orElseThrow(() ->  new IllegalArgumentException("Empresa no existe"));
	}
}
