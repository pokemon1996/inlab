package com.example.duobot.inlab.service;

import java.util.List;

import com.example.duobot.inlab.dto.CampaignTypeDTORs;
import com.example.duobot.inlab.dto.client.dashboard.CampaignTypeUserDTORs;

public interface CampaignTypeService {

	List<CampaignTypeDTORs> findAllCampaigns();
	
	List<CampaignTypeUserDTORs> findCampaignsTypeByUser(String userAssigned);
	
	String getBackgroundImageUrl(String logo);
	
}
