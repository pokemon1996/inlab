package com.example.duobot.inlab.service;

import java.util.List;

import com.example.duobot.inlab.dto.ResourceTypeDTORs;

public interface ResourceTypeService {

	List<ResourceTypeDTORs> findAllResourceTypes();
	
}
