package com.example.duobot.inlab.service;

import java.util.List;
import java.util.UUID;

import org.springframework.web.multipart.MultipartFile;

import com.example.duobot.inlab.dto.EnterprisesDTORq;
import com.example.duobot.inlab.dto.EnterprisesDTORs;
import com.example.duobot.inlab.dto.EnterprisesDetailDTORs;

public interface EnterpriseService {

	void createEnterprise(
		final EnterprisesDTORq enterprisesDTOIn, 
		final MultipartFile logo);
	
	void updateEnterprise(
			final EnterprisesDTORq enterprisesDTOIn, 
			final MultipartFile logo);
	
	List<EnterprisesDTORs> findAllEnterprises();
	
	EnterprisesDetailDTORs getDetailEnterprise(UUID enterpriseId);
	
}
