package com.example.duobot.inlab.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringTokenizer;
import org.apache.commons.text.matcher.StringMatcher;
import org.springframework.stereotype.Service;

import com.example.duobot.inlab.controller.form.AnswerResponse;
import com.example.duobot.inlab.controller.form.Tag;
import com.example.duobot.inlab.dao.CampaignRepository;
import com.example.duobot.inlab.dao.ProhibitedWordRepository;
import com.example.duobot.inlab.dao.QuestionRepository;
import com.example.duobot.inlab.model.Campaign;
import com.example.duobot.inlab.model.CampaignType;
import com.example.duobot.inlab.service.QuestionService;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@RequiredArgsConstructor
public class QuestionServiceImpl implements QuestionService{
	
	private final CampaignRepository campaignRepository;
	
	private final QuestionRepository questionRepository;

	private final ProhibitedWordRepository prohibitedWordRepository;
	
	private final StringMatcher matcherAnswers;
	
	public List<AnswerResponse> getAnswersPollByCampaign(final Integer campaignId){
		return this.campaignRepository.findById(campaignId)
			.map(Campaign::getInsightId)
			.map(this::getAnswersPollById)
			.orElseGet(Collections::emptyList);
	}
	
	public List<AnswerResponse> getAnswersPollByCampaignTypeAndUser(final UUID campaignTypeId, final String username){
		
		final List<AnswerResponse> responses = new ArrayList<>();
		
		this.campaignRepository.findByCampaignTypeAndAssignedUserOrderByStartCampaignDateDesc(
			CampaignType.builder().campaingIdType(campaignTypeId).build(), username)
			.stream()
			.map(Campaign::getInsightId)
			.map(this::getAnswersPollById)
			.forEach(responses::addAll);
		
		return responses;
	}
	
	public List<AnswerResponse> getAnswersPollById(final Integer pollId){
		final List<AnswerResponse> response = new ArrayList<AnswerResponse>();
		
		final Set<String> badWords = prohibitedWordRepository.findAllSet();
		
		final String badWordsAsString = badWords.stream()
			.collect(Collectors.joining(","));

		return this.questionRepository.findByPollPollId(pollId)
			.stream()
			.map(question -> {
				
				final AnswerResponse answerResponse = new AnswerResponse();
				
				answerResponse.setQuestionId(question.getQuestionId());
				answerResponse.setQuestionName(question.getQuestion());

				String word = question.getAnswers().stream().map(answer -> answer.getAnswer()).collect(Collectors.joining(StringUtils.SPACE));

				final StringTokenizer tokenizer = new StringTokenizer(word);
				
				tokenizer.setQuoteMatcher(matcherAnswers);
				
				final Map<String, Long> counts = tokenizer.getTokenList()
					.stream()
					.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
				
				final List<Tag> tags = counts.keySet().stream()
					.filter(keyWord -> !badWords.contains(keyWord))
					.map(keyWordPassed -> Tag.builder()
						.text(keyWordPassed)
						.weight(counts.get(keyWordPassed))
						.build())
					.collect(Collectors.toList());
				
				answerResponse.setTags(tags);
				
				return answerResponse;
				
			})
			.collect(Collectors.toList());	
	}
	/*
	List<AnswerResponse> response = new ArrayList<AnswerResponse>();
	List<ProhibitedWord> badWords = prohibitedWordService.findAll();
	List<Question> questions = questionRepository.findByPollPollId(pollId);
	for(Question question : questions) {
		AnswerResponse answerResponse = new AnswerResponse();
		answerResponse.setQuestionId(question.getQuestionId());
		answerResponse.setQuestionName(question.getQuestion());
		String word = question.getAnswers().stream().map(answer -> answer.getAnswer()).collect(Collectors.joining(","));
		String badWordsAsString = badWords.stream().map(badWord -> badWord.getWord()).collect(Collectors.joining(","));
		Map<String, Integer> counts = Arrays.asList(word.split("/[ '\\-\\(\\)\\*\":;\\[\\]|{},.!?]+/")).parallelStream().
	            collect(Collectors.toConcurrentMap(
	                w -> w.toString(), w -> 1, Integer::sum));
		
		for(String key : counts.keySet()) {
			boolean isBadWord = badWords.stream()
		            .anyMatch(badWord -> badWord.getWord().equals(key));
			if(isBadWord) {
				continue;
			}
			answerResponse.getTags().add(new Tag(key, counts.get(key), word, badWordsAsString));
		}
		response.add(answerResponse);
	}
*/
}