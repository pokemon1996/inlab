package com.example.duobot.inlab.service;

import java.util.List;
import java.util.UUID;

import com.example.duobot.inlab.dto.UserCreationDTORq;
import com.example.duobot.inlab.dto.UserListDTORs;

public interface UserService {

	void createUser(UserCreationDTORq creationDTORq);
	
	void updateUser(UserCreationDTORq creationDTORq);
	
	List<UserListDTORs> getListUsers();
	
	List<UserListDTORs> findByEnterprise(UUID enterpriseId);
}
