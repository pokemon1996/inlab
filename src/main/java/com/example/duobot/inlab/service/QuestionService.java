package com.example.duobot.inlab.service;

import java.util.List;
import java.util.UUID;

import com.example.duobot.inlab.controller.form.AnswerResponse;

public interface QuestionService {

	List<AnswerResponse> getAnswersPollByCampaign(Integer campaignId);
	
	List<AnswerResponse> getAnswersPollById(Integer pollId);
	
	List<AnswerResponse> getAnswersPollByCampaignTypeAndUser(UUID campaignTypeId, String username);
}
