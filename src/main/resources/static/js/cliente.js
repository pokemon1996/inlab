const formatDDMMYYYY = 'DD/MM/YYYY';
const DEFAULT = 'DEFAULT';

$(document).ready(() => {
		
	const getCampaignTypesByUser = () => {
		return $.ajax({
			type: 'GET',
	    	url: `/api/v1/campaign-type/user/${username}`,
	    	data: {},
	        contentType: 'application/json',
		});
	};
	
	const getCampaignsByUserAndType = (campaignTypeId) => {
		return $.ajax({
			type: 'GET',
	    	url: `/api/v1/campaigns/type/${campaignTypeId}/user/${username}`,
	    	data: {},
	        contentType: 'application/json',
		});
	};
	
	const getResourcesCampaign = () => {
		return $.ajax({
			type: 'GET',
	    	url: `/api/v1/resource-campaign/campaign/${campaign}`,
	    	data: {},
	        contentType: 'application/json',
		});
	};
	
	const getResourcesCampaignByCampaignTypeAndUsername = (campaignTypeId) => {
		return $.ajax({
			type: 'GET',
	    	url: `/api/v1/resource-campaign/campaign-type/${campaignTypeId}/user/${username}`,
	    	data: {},
	        contentType: 'application/json',
		});
	};
	
	const getGalleriesCampaign = () => {
		return $.ajax({
			type: 'GET',
	    	url: `/api/v1/galleries/campaign/${campaign}`,
	    	data: {},
	        contentType: 'application/json',
		});
	};
	
	const getGalleriesByCampaignTypeAndUsername = (campaignTypeId) => {
		return $.ajax({
			type: 'GET',
	    	url: `/api/v1/galleries/campaign-type/${campaignTypeId}/user/${username}`,
	    	data: {},
	        contentType: 'application/json',
		});
	};
	
	const getAnswersPollByCampaignTypeAndUser = (campaignTypeId) => {
		return $.ajax({
			type: 'GET',
	    	url: `/api/v1/answer/campaign-type/${campaignTypeId}/user/${username}`,
	    	data: {},
	        contentType: 'application/json',
		});
	};
	
	const getAnswersPollByCampaign = () => {
		return $.ajax({
			type: 'GET',
	    	url: `/api/v1/answer/campaign/${campaign}`,
	    	data: {},
	        contentType: 'application/json',
		});
	};
	
	const carouselCampaignTypes = $('#carouselCampaignTypes .carousel-inner');
	
	if(carouselCampaignTypes.length){
		const rowItem = $('<div class="row"></div>');
		let rowItemContext = rowItem.clone();
		
		getCampaignTypesByUser()
			.done(data => {
				data && data.forEach((element, index) => {
					const nameMethodology = (DEFAULT !== element.name && element.name) || 'Mis estudios';
					rowItemContext.append(
						`<div class="col-md-2">
							<div class="card mb-2" ${!element.isActive && 'data-toggle="tooltip" data-placement="top" title="Aún no usas esta Metodología, conócela."'}>
			  					<a class="btn btn-sm btn-lighter ${(element.isActive && 'campaigns') || ''}" href="#" 
			  						data-methodology="${element.campaingIdType}" 
			  						data-name="${element.name}" 
			  						data-description="${element.description}"
			  						data-background="${element.backgroundImageUrl}"
			  						${!element.isActive && 'data-toggle="modal" data-target="#modal-met"'}>
									<img src="${element.backgroundImageUrl}" class="card-img-top ${!element.isActive && 'opacity-4'}" alt="${nameMethodology}"" title="${nameMethodology}">							
								</a>
							</div>
						</div>`);

					if((0 === (index + 1) % 6) || (data.length === index + 1)) {
						carouselCampaignTypes.append($('<div class="carousel-item"></div>').append(rowItemContext));
						rowItemContext = rowItem.clone();
					}
				});
				
				const firstItem = carouselCampaignTypes.find('.carousel-item:first-child');

				firstItem.addClass('active');
				firstItem.find('a.campaigns:first').click();

				$('[data-toggle="tooltip"]').tooltip();
			});
			
		const modalComponentMethodology = $('#modal-met');

		if(modalComponentMethodology.length) {
			modalComponentMethodology.on('hidden.bs.modal', (event) => {
			
				const modalBody = $(event.target).find('.modal-body');

				modalBody.empty();
			});
		
			modalComponentMethodology.on('shown.bs.modal', (event) => {
	
				const modalBody = $(event.target).find('.modal-body');
				const modalTitle = $(event.target).find('.modal-title');
				const linkMethodology = $(event.relatedTarget);
					
				modalTitle.text(linkMethodology.data('name'));
				modalBody.html(`
	            	<div class="row">
		            	<div class="col-md-4">
		                	<img src="${linkMethodology.data('background')}" width="100%">
		            	</div>
		            	<div class="col-md-8 p-2">
		                	<p class="text-center">${linkMethodology.data('description')}</p>
		            	</div>
	            	</div>`);
			});
		}
		
		$(document).on('click', '#carouselCampaignTypes a.campaigns', (event) => {
			event.preventDefault();

			const methodology = event.currentTarget.dataset.methodology;
			const defaultMethodology = $('#defaultMethodology');
			const specificMethodology = $('#specificMethodology');

			if ('DEFAULT' !== event.currentTarget.dataset.name) {
				
				const bodyTableCampaigns = $('#tbcampaigns tbody');
		
				bodyTableCampaigns.empty();
				
				getCampaignsByUserAndType(methodology)
					.done(data => {
						const currentDate = new Date();
						
						data && data.forEach(element => {
							
							const endCampaignDate = element.endCampaignDate && moment(element.endCampaignDate, formatDDMMYYYY).toDate();
							
							bodyTableCampaigns.append(`
								<tr>
				                    <th scope="row">
				                      <div class="media align-items-center">
				                        <div class="media-body">
				                          <span class="name mb-0 text-sm">${element.campaignName}</span>
				                        </div>
				                      </div>
				                    </th>
				                    <td>
				                      <span class="badge badge-dot mr-4">
				                      	${(endCampaignDate && currentDate <= endCampaignDate && 
				                      		'<i class="bg-warning"></i><span class="status">En curso</span>') || 
				                      		'<i class="bg-success"></i><span class="status">Finalizado</span>'
				                    	}
				                      </span>
				                    </td>
				                    <td>
				                      <span class="badge badge-dot mr-4">
				                        <span class="status">${element.startCampaignDate || 'Sin Información'}</span>
				                      </span>
				                    </td>
				                    <td>
				                      <span class="badge badge-dot mr-4">
				                        <span class="status">${element.endCampaignDate || 'Sin Información'}</span>
				                      </span>
				                    </td>
				                    <td class="text-center">
				                        <a class="btn btn-primary btn-sm" href="/detalleCampana/${element.campaignId}" type="button">
				                        	<span class="btn-inner--icon"><i class="fas fa-caret-right"></i></span>
			    							<span class="btn-inner--text">Ver</span>
				                        </a>
				                    </td>
		 						</tr>`
							);
						});
						
						defaultMethodology.addClass('d-none');
						specificMethodology.removeClass('d-none');
						
					});
			} else {
				
				defaultMethodology.children('*:not(".not-cleared")').remove();

				getResourcesCampaignByCampaignTypeAndUsername(methodology)
					.done(data => {
						generateComponentResourcesCampaign(defaultMethodology, data);
					});
				
				getGalleriesByCampaignTypeAndUsername(methodology)
					.done(data => {
						generateComponentGalleries($('#carouselGallerieCampaignDefault'), data);
					});
				
				getAnswersPollByCampaignTypeAndUser(methodology)
					.done(data => {
						generateComponentInsight($('#carouselInsightsCampaignDefault'), data);
					});
					
				defaultMethodology.removeClass('d-none');
				specificMethodology.addClass('d-none');
			}
		});
	}
	
	const resourcesCampaign = $('#resourcesCampaign');
	
	if (resourcesCampaign.length) {
		getResourcesCampaign()
			.done(data => {
				generateComponentResourcesCampaign(resourcesCampaign, data);
			});
	}
	
	const generateComponentResourcesCampaign = (parent, data) => {
		
		const keysResources = Object.keys(data) || [];

		keysResources.forEach((resourceType, index) => {
			parent.append(
				`<div class="col-xl-6">
		          <div class="card">
		            <div class="card-header bg-transparent">
		              <div class="row align-items-center">
		                <div class="col-xl-6">
		                  <h5 class="h3 mb-0">${resourceType}</h5>
		                </div>
		                <div class="col-xl-6 text-right">
			              <div class="${(1 === data[resourceType].length && 'd-none') || 'd-inline-block'}">
				             <a class="button-carousel" href="#carouselResource${index}" data-slide="prev" title="Atrás"><i class="fa fa-chevron-left"></i></a>
				             <a class="button-carousel" href="#carouselResource${index}" data-slide="next" title="Adelante"><i class="fa fa-chevron-right"></i></a>
				          </div>
						  <a data-toggle="modal" data-target="#modal-amp" style="cursor:pointer!important;" title="Ampliar">
	              			<i class="fa fa-window-maximize buttons-right-carousel" aria-hidden="true"></i>
	          	  		  </a>
		                </div>
		              </div>
		            </div>
		            <div class="card-body pt-2" data-resourcetype="${resourceType}">
						<div id="carouselResource${index}" class="carousel slide carousel-multi-item carouselResource" data-ride="carousel">
							<div class="carousel-inner">
							${
								data[resourceType].map((resource, index) => {
									return `<div class="carousel-item">
										${'Sala' !== resourceType &&
											`<iframe class="resourceTypeCampaign" src="${resource.resourceUrl}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen allow="microphone; camera"></iframe>`
											|| 
											`<a class="resourceTypeCampaign display-1 w-100 d-inline-flex align-items-center justify-content-center" href="${resource.resourceUrl}" target="_blank" title="Sala ${index + 1}">
												<i class="fa fa-video"></i>&nbsp;Sala ${index + 1}
											</a>`
										}
									</div>`;
								}).join('')
							}
							</div>
		          		</div>
		            </div>
		          </div>
		        </div>`);
		});
		parent.find('.carousel-inner .carousel-item:first-child').addClass('active');
	};
	
	const carouselGallerieCampaign = $('#carouselGallerieCampaign');
	
	if(carouselGallerieCampaign.length) {
		getGalleriesCampaign()
			.done(data => {
				generateComponentGalleries(carouselGallerieCampaign, data);
			});
	}
	
	const generateComponentGalleries = (parent, data) => {
		
		const carouselGallerie = parent.find('.carousel-inner');
		
		carouselGallerie.empty();
		
		if(data && data.length){
			data.forEach(element => {
				carouselGallerie.append(
					`<div class="carousel-item">
						<img class="resourceTypeCampaign" src="${element.image}"/>
					</div>`);
				});
					
			parent.removeClass('d-none');
				
			carouselGallerie.find('.carousel-item:first-child').addClass('active');
		}
	}
	
	const carouselInsightsCampaign = $('#carouselInsightsCampaign');
	
	if(carouselInsightsCampaign.length) {
		getAnswersPollByCampaign()
			.done(data => {
				generateComponentInsight(carouselInsightsCampaign, data);
			});
	}
	
	const generateComponentInsight = (parent, data) => {
		
		const carouselInsight = parent.find('.carousel-inner');
		
		carouselInsight.empty();
		
		if(data && data.length){
			data.forEach((element, index) => {
				carouselInsight.append(
					`<div class="carousel-item">
						<div class="resourceTypeCampaign">
							<h5 style="height: 5%;">${element.questionName}</h5>
							<div id="words${index}" class="mx-auto" style="width: 400px; height: 300px;"></div>
						</div>
					</div>`);
										
				$(`#words${index}`).jQCloud(element.tags, {
					delay: 75
				});				
			});

			parent.removeClass('d-none');
				
			carouselInsight.find('.carousel-item:first-child').addClass('active');
		}
	}
			
	const modalComponent = $('#modal-amp');

	if(modalComponent.length) {
		
		modalComponent.on('hidden.bs.modal', (event) => {
			
			const modalBody = $(event.target).find('.modal-body');

			modalBody.empty();
		});
		
		modalComponent.on('shown.bs.modal', (event) => {

			const modalBody = $(event.target).find('.modal-body');
			const modalTitle = $(event.target).find('.modal-title');
			
			const cardBodyParent = $(event.relatedTarget)
				.parents('.card')
				.find('.card-body');
				
			const cardBodySelected = cardBodyParent
				.children()
				.clone();
				
			const resourceType = cardBodyParent.data('resourcetype');

  			if(cardBodySelected.length) {
				
				const carousel = cardBodySelected.hasClass('carousel');

				if(carousel){
					
					cardBodySelected.addClass('h-100');
					cardBodySelected.attr('id', 'carouselModal');
					cardBodySelected.find('.carousel-inner').addClass('text-center h-100');
					cardBodySelected.find('.carousel-item').addClass('h-100');
					cardBodySelected.find('.resourceTypeCampaign').addClass('h-100');
					cardBodySelected.find('img.resourceTypeCampaign').addClass('img-resourceTypeCampaign');
				}
				
				modalTitle.text(`Vista ampliada de ${resourceType}`);
				modalBody.html(cardBodySelected);
			}
		});
	}
});