package com.example.duobot.inlab.mocks;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

import org.springframework.mock.web.MockMultipartFile;
import org.springframework.util.ResourceUtils;

import com.example.duobot.inlab.dto.EnterprisesDTORq;
import com.example.duobot.inlab.model.CampaignType;
import com.example.duobot.inlab.model.Enterprises;
import com.example.duobot.inlab.model.ResourceType;
import com.example.duobot.inlab.model.User;

public final class DummyMocks {

	private DummyMocks() {}
	
	public static final String USER_NAME_DEMO = "demo@inlab.com";
	
	public static final String RESOURCES_TMP = "src/test/resources/tmp";
	
	public static User getDemoUser() {
		return User.builder()
			.userId(19)
			.username("demo@inlab.com")
			.completeName("demo")
			.password("$2a$10$gh7xa7nUNcm1fFhpu4jGEeadheYb/IC1aB46L.fMPlQfPnYEYAEly")
			.build();
	}
	
	public static Enterprises getEnterpriseCorpCapsule() {
		return Enterprises.builder()
			.enterpriseId(UUID.fromString("1A0F0B78-73B7-2A4C-8029-211311519CA2"))
			.identification("800541321")
			.dv(9)
			.businessName("Corporacion CAPSULE")
			.build();
	}
	
	public static EnterprisesDTORq getEnterpriseCorpCapsuleDTO() {
		return EnterprisesDTORq.builder()
			.identification("800541321")
			.dv(9)
			.businessName("Corporacion CAPSULE")
			.backgroundColorHex("4C2882")
			.titleColorHex("FFFFFF")
			.build();
	}
	
	public static Enterprises getEnterpriseCorpUmbrella() {
		return Enterprises.builder()
			.enterpriseId(UUID.fromString("8BBE91BE-0921-2942-A347-4BF107C33DFA"))
			.identification("901200365")
			.dv(6)
			.businessName("Corporacion UMBRELLA")
			.build();
	}
	
	public static CampaignType getCampaignTypeInlab() {
		return CampaignType.builder()
			.campaingIdType(UUID.fromString("8709866A-E392-422F-B87E-BCE8D5058CB0"))
			.name("INLAB")
			.build();
	}
	
	public static CampaignType getCampaignTypeConsultoria() {
		return CampaignType.builder()
			.campaingIdType(UUID.fromString("7D31AB2B-951C-4D90-BF02-07DE22DCBFDE"))
			.name("CONSULTORIA")
			.build();
	}
	
	public static ResourceType getResourceTypeURLVideo() {
		return ResourceType.builder()
			.resourceTypeId(UUID.fromString("19742B21-EE68-40DE-8332-23F1A100EC00"))
			.name("URL Video")
			.build();
	}
	
	public static ResourceType getResourceTypeURLPowerBI() {
		return ResourceType.builder()
			.resourceTypeId(UUID.fromString("9FFAC309-D532-43F8-9A73-5A0EC97D1F47"))
			.name("URL Power BI")
			.build();
	}
	
	public static MockMultipartFile getLogoCapsuleCorp() {
		try {
			return new MockMultipartFile(
				"logo-corp-capsule.png", 
				null, 
				"image/png", 
				new FileInputStream(ResourceUtils.getFile("classpath:images/logo-corp-capsule.png")));
		} catch (IOException e) {
			return null;
		}
	}
}
