package com.example.duobot.inlab.service.impl;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.example.duobot.inlab.dao.CampaignTypeRepository;
import com.example.duobot.inlab.dto.CampaignTypeDTORs;
import com.example.duobot.inlab.mapper.CampaignTypeMapper;
//import com.example.duobot.inlab.mapper.CampaignTypeMapperImpl;
import com.example.duobot.inlab.mocks.DummyMocks;

class CampaignTypeServiceImplTest {

	private CampaignTypeRepository campaignTypeRepository;
	
	private CampaignTypeMapper campaignTypeMapper;
	
	private CampaignTypeServiceImpl campaignTypeServiceImpl;
	
	@BeforeEach
	void setUp() {
		this.campaignTypeRepository = Mockito.mock(CampaignTypeRepository.class);
		this.campaignTypeMapper =  Mockito.mock(CampaignTypeMapper.class);;
		this.campaignTypeServiceImpl = new CampaignTypeServiceImpl(this.campaignTypeMapper, this.campaignTypeRepository);
	}
	
	@Test
	void findAllCampaignsIsDone() {
		Mockito.when(this.campaignTypeRepository.findAll())
			.thenReturn(Arrays.asList(DummyMocks.getCampaignTypeInlab(), DummyMocks.getCampaignTypeConsultoria()));
		
		final List<CampaignTypeDTORs> lsCampaignTypes = this.campaignTypeServiceImpl.findAllCampaigns();
		
		Assertions.assertFalse(lsCampaignTypes::isEmpty);
	}
}
