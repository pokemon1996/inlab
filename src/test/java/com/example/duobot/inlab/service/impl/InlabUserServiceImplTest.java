package com.example.duobot.inlab.service.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.example.duobot.inlab.dao.InlabUserRepository;
import com.example.duobot.inlab.mapper.UserMapper;
import com.example.duobot.inlab.mapper.UserMapperImpl;
import com.example.duobot.inlab.mocks.DummyMocks;
import com.example.duobot.inlab.model.User;
import com.example.duobot.inlab.service.InlabUserService;

class InlabUserServiceImplTest {

	private UserMapper userMapper;
	
	private InlabUserRepository inlabUserRepository;

	private InlabUserService inlabUserService;
	
	@BeforeEach
	void setUp() {
		this.inlabUserRepository = Mockito.mock(InlabUserRepository.class);
		this.userMapper = new UserMapperImpl();
		this.inlabUserService = new InlabUserServiceImpl(this.userMapper, this.inlabUserRepository);
	}
	
	@Test
	void findByUsernameIsDone() {
		Mockito.when(this.inlabUserRepository.findByUsername(Mockito.anyString()))
			.thenReturn(DummyMocks.getDemoUser());
		
		final User demoUser = this.inlabUserService.findByUsername(DummyMocks.USER_NAME_DEMO);
		
		Assertions.assertEquals(DummyMocks.USER_NAME_DEMO, demoUser.getUsername());
	}
}
