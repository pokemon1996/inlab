package com.example.duobot.inlab.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import com.example.duobot.inlab.dao.EnterpriseLayoutRepository;
import com.example.duobot.inlab.dao.EnterprisesRepository;
import com.example.duobot.inlab.dto.EnterprisesDTORs;
import com.example.duobot.inlab.mapper.EnterpriseMapper;
import com.example.duobot.inlab.mapper.EnterpriseMapperImpl;
import com.example.duobot.inlab.mocks.DummyMocks;
import com.example.duobot.inlab.service.EnterpriseService;

class EnterpriseServiceImplTest {

	private EnterpriseMapper enterpriseMapper;
	
	private EnterprisesRepository enterprisesRepository;
	
	private EnterpriseLayoutRepository enterpriseLayoutRepository;
	
	private EnterpriseService enterpriseService;
	
	@BeforeEach
	void setUp() {
		this.enterprisesRepository = Mockito.mock(EnterprisesRepository.class);
		this.enterpriseLayoutRepository = Mockito.mock(EnterpriseLayoutRepository.class);
		this.enterpriseMapper = new EnterpriseMapperImpl();
		this.enterpriseService = new EnterpriseServiceImpl(
			this.enterpriseMapper, 
			this.enterprisesRepository, 
			this.enterpriseLayoutRepository);
		
		ReflectionTestUtils.setField(this.enterpriseService, "pathLogos", DummyMocks.RESOURCES_TMP);
	}
	
	@Test
	void findAllEnterprisesIsDone() {
		Mockito.when(this.enterprisesRepository.findAll())
			.thenReturn(Arrays.asList(DummyMocks.getEnterpriseCorpCapsule(), DummyMocks.getEnterpriseCorpUmbrella()));
	
		final List<EnterprisesDTORs> lsEnterprises = this.enterpriseService.findAllEnterprises();
		
		Assertions.assertFalse(lsEnterprises::isEmpty);
	}
	
	@Test
	void createEnterpriseIfExists() {
		Mockito.when(this.enterprisesRepository.findByIdentification(Mockito.anyString()))
			.thenReturn(Optional.of(DummyMocks.getEnterpriseCorpCapsule()));
		
		Assertions.assertThrows(IllegalArgumentException.class, 
			() -> this.enterpriseService.createEnterprise(DummyMocks.getEnterpriseCorpCapsuleDTO(), DummyMocks.getLogoCapsuleCorp()));	
	}
	
}
