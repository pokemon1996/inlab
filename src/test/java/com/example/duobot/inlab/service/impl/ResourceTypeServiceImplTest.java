package com.example.duobot.inlab.service.impl;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.example.duobot.inlab.dao.ResourceTypeRepository;
import com.example.duobot.inlab.dto.ResourceTypeDTORs;
import com.example.duobot.inlab.mapper.ResourceTypeMapper;
import com.example.duobot.inlab.mapper.ResourceTypeMapperImpl;
import com.example.duobot.inlab.mocks.DummyMocks;
import com.example.duobot.inlab.service.ResourceTypeService;

class ResourceTypeServiceImplTest {

	private ResourceTypeMapper resourceTypeMapper;
	
	private ResourceTypeRepository resourceTypeRepository;
	
	private ResourceTypeService resourceTypeService;
	
	@BeforeEach
	void setUp() {
		this.resourceTypeRepository = Mockito.mock(ResourceTypeRepository.class);
		this.resourceTypeMapper = new ResourceTypeMapperImpl();
		this.resourceTypeService = new ResourceTypeServiceImpl(resourceTypeMapper, resourceTypeRepository);
	}
	
	@Test
	void findAllResourceTypesIsDone() {
		Mockito.when(this.resourceTypeRepository.findAll())
			.thenReturn(Arrays.asList(DummyMocks.getResourceTypeURLPowerBI(), DummyMocks.getResourceTypeURLVideo()));
	
		
		final List<ResourceTypeDTORs> lsResourceTypes = this.resourceTypeService.findAllResourceTypes();
		
		Assertions.assertFalse(lsResourceTypes::isEmpty);
	}
}
