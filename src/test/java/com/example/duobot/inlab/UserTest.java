package com.example.duobot.inlab;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.duobot.inlab.dao.InlabUserRepository;
import com.example.duobot.inlab.model.User;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class UserTest {
	
	@Autowired
	InlabUserRepository userService;
	
	@Test
	public void getUsers(){
		Iterable<User> users = userService.findAll();
		assertNotNull(users);
		for(User user : users) {
			System.out.println(user.getUsername() + " role: " + user.getRoleName());
		}
	}

}
